<?
#require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->SetTitle("Авто-импорт скидок");
?>
<?php
# автоматический импорт из загруженных по FTP файлов
define('AUTO_IMPORT', true);
define('AUTO_IMPORT_DISCOUNTS', true);
?>
<?$APPLICATION->IncludeComponent(
	"uniweb:base.import",
	".default",
	Array(
		"IBLOCK_ID" => "7"
	)
);?>
<?
#require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>