<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

#print_r($arResult);
?>

<?if(!defined('AUTO_IMPORT')):?>
	<hr>
	<form method="post" enctype="multipart/form-data">
		<h3>Выгрузка товаров</h3>
		<input type="file" name="goodsXml">
		<input type="submit" name="xmlSubmit" value="Выгрузить товары">
	</form>
	<br><br>

	<hr>

	<form method="post" enctype="multipart/form-data">
		<h3>Выгрузка скидок</h3>
		<input type="file" name="discountXml">
		<input type="submit" name="xmlSubmit" value="Выгрузить скидки">
	</form>
	<br><br>

	<hr>

	<form method="post" enctype="multipart/form-data">
		<h3>Выгрузка пользователей</h3>
		<input type="file" name="usersXml">
		<input type="submit" name="xmlSubmit" value="Выгрузить пользователей">
	</form>
<?endif;?>