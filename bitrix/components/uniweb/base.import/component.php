<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$iblockId = intval($arParams['IBLOCK_ID']);
CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');
CModule::IncludeModule('sale');

function pr($array) {
	echo '<pre>'.print_r($array,1).'</pre>';
}



class CatImport {
	private $_iblockId;
	private $_cache = array();
	private $_discountAdded = 0;
	public $auto = false; # флаг автоматического импорта
	public $productsXmlPath;
	public $discountsXmlPath;
	public $usersXmlPath;

	# сообщения об успешных действиях
	private $_successes = array();

	# ошибки
	private $_errors = array();

	public function __construct() {
		$this->_iblockId = 3;
		@ini_set('max_execution_time', 60*5);
		@set_time_limit(60*5);

		$this->_cache['bitrixObjects'] = array();
	}

	/**
	 * Возвращает объект одного из классов битрикса (чтобы не создавать новые объекты в разных методах)
	 * @param $className
	 * @return mixed
	 */
	private function getBitrixObject($className) {
		if(isset($this->_cache['bitrixObjects'][$className])) {
			return $this->_cache['bitrixObjects'][$className];
		}
		else {
			$object = new $className;
			$this->_cache['bitrixObjects'][$className] = $object;
			return $object;
		}
	}

	/**
	 * Выводит сообщения импорта
	 */
	private function showMessages() {
		if(!defined('AUTO_IMPORT')) {
			if(sizeof($this->_successes)) {
				echo '<div class="importSuccess">'.implode('<br>', $this->_successes).'</div>';
			}
			if(sizeof($this->_errors)) {
				echo '<div class="importErrors">'.implode('<br>', $this->_errors).'</div>';
			}
		}
		else {
			if(sizeof($this->_errors)) {
				echo 'ERROR';
			}
			else {
				echo 'OK';
			}
		}
		# обнуляем сообщения
		$this->_successes = $this->_errors = array();
	}

	/**
	 * Обновляем варианты свйоств инфоблока типа "список"
	 * @param $propId
	 */
	private function updateEnumProp($propId, $xmlNode, $propName) {
		if(!sizeof($xmlNode)) {
			$this->_errors[] = 'В файле выгрузки не найдено: '.$propName;
			return;
		}

		$ipObject = new CIBlockProperty;
		$ipeObject = new CIBlockPropertyEnum;
		$added = $updated = $deleted = 0;
		$variants= array();

		# перегоняем $xmlNode в ассоциативный массив
		foreach($xmlNode as $variant) {
			$variants[(string)$variant['id']] = (string)$variant['value'];
		}

		# перечисляем все варианты из XML и проверяем их в CMS. Если в CMS таких нет - заливаем, если есть - обновляем
		foreach($variants as $varId=>$varValue) {
			# ищем такой вариант в CMS
			$cmsVariant = $ipeObject->GetList(array(), array('EXTERNAL_ID'=>$varId));
			if($cmsVariant->SelectedRowsCount()) {
				# обновляем
				$cmsVariant = $cmsVariant->Fetch();
				if($ipeObject->Update($cmsVariant['ID'], array('VALUE'=>$varValue))) {
					$updated++;
				}
				else {
					$this->_errors[] = 'Ошибка обновления варианта свойства "'.$propName.'", id='.$cmsVariant['XML_ID'].': ';
				}
			}
			else {
				# заливаем
				if($ipeObject->Add(array(
					'PROPERTY_ID' => $propId,
					'XML_ID' => $varId,
					'VALUE' => $varValue
				))) {
					$added++;
				}
				else {
					$this->_errors[] = 'Ошибка внесения варианта свойства "'.$propName.'", id='.$varId.': ';
				}
			}
		}

		# получаем значение всех вариантов свойств и удаляем те, которых нет в XML
		$rsVariants = $ipObject->GetPropertyEnum($propId);
		if($rsVariants->SelectedRowsCount()) {
			while($arVariant = $rsVariants->Fetch()) {
				if(!array_key_exists($arVariant['XML_ID'], $variants)) {
					# удаляем вариант из CMS
					if($ipeObject->Delete($arVariant['ID'])) {
						$deleted++;
					}
					else {
						$this->_errors[] = 'Ошибка удаления варианта свойства "'.$propName.'", id='.$arVariant['XML_ID'].': ';
					}
				}
			}
		}
		$this->_successes[] = $propName.': добавлено - '.$added.', обновлено - '.$updated.', удалено - '.$deleted;
	}

	/**
	 * Возвращает ид варианта свойства инфоблока по его XML ID
	 * @param $propId
	 * @param $xmlId
	 * @return mixed
	 */
	private function getEnumVariantId($propId, $xmlId) {
		if(isset($this->_cache['prop-'.$propId])) {
			$arVariants = $this->_cache['prop-'.$propId];
		}
		else {
			$arVariants = array();
			$rsVariants = CIBlockProperty::GetPropertyEnum($propId);
			if($rsVariants->SelectedRowsCount()) {
				while($arVariant = $rsVariants->Fetch()) {
					$arVariants[$arVariant['XML_ID']] = $arVariant['ID'];
				}
			}
			$this->_cache['prop-'.$propId] = $arVariants;
		}
		if(array_key_exists($xmlId, $arVariants)) {
			return $arVariants[$xmlId];
		}
		else {
			return false;
		}
	}

	/**
	 * Возвращает ид товара по его XML ID
	 */
	private function getGoodId($article) {
		$rsGoods = CIBlockElement::GetList(
			array(),
			array(
				'IBLOCK_ID' => $this->_iblockId,
				'=PROPERTY_ARTICLE' => $article
			)
		);
		if($rsGoods->SelectedRowsCount()) {
			$arGood = $rsGoods->Fetch();
			return $arGood['ID'];
		}
		return false;
	}

	/**
	 * Импортирует товары
	 */
	public function importGoods() {
		# проверяем XML
		if(!$this->checkXml($this->productsXmlPath)) {
			$this->showMessages();
			return;
		}

		$xml = simplexml_load_file($this->productsXmlPath);
		$ieObject = new CIBlockElement;
		$priceObject = new CPrice;
		$catObject = new CCatalogProduct;

		# импортируем категории скидок
		$this->updateEnumProp(29, $xml->xpath('//cat_discount/catdiscname'), 'Категории скидок');

		# импортируем категории
		$this->updateEnumProp(28, $xml->xpath('//categories/catname'), 'Категории');

		# импортируем бренды
		$this->updateEnumProp(30, $xml->xpath('//brands/brand'), 'Бренды');


		# импортируем связи котегорий и брендов
		$cats = $xml->xpath('//categories/catname');
		# первым делом составляем список брендов
		$arBrands = array();
		foreach($cats as $cat) {
			$catId = (string)$cat['id'];
			$catBrands = explode(',', (string)$cat['brand_ids']);
			if(!is_array($catBrands)) continue;
			foreach($catBrands as $catBrand) {
				if(!array_key_exists($catBrand, $arBrands)) {
					$arBrands[$catBrand] = array();
				}
				$arBrands[$catBrand][] = $catId;
			}
		}
		$arBrands = json_encode($arBrands);
		COption::SetOptionString('main', 'cat_relations', $arBrands, 'Связи категорий и брендов');

		/*$arBrands = COption::GetOptionString('main', 'cat_relations');
		pr(json_decode($arBrands, true));*/



		# импортируем товары
		$goods = $xml->xpath('//elements/elem');
		if(!sizeof($goods)) {
			$this->_errors[] = 'В файле выгрузки не найдены товары';
		}
		else {
			/*
			 * Уникальный ид товара - артикул. Проверяем на сайте - если товар с текущим артикулом есть,
			 * обновляем его свойства. Иначе добавляем товар. Всем товарам изначально проставляем 0 в свойство FROM_XML_TMP,
			 * затем каждому обновлённому или добавленному товару проставляем 1. В конце выгрузки все товары, у которых FROM_XML_TMP=0
			 * деактивируем (или удаляем) - этих товаров нет в файле выгрузки.
			 */

			$added = $updated = $deleted = 0;

			# проставляем FROM_XML_TMP=0 у всех элементов
			$rsElems = $ieObject->GetList(array(), array('IBLOCK_ID'=>$this->_iblockId));
			if($rsElems->SelectedRowsCount()) {
				while($arElem = $rsElems->Fetch()) {
					$ieObject->SetPropertyValuesEx(
						$arElem['ID'],
						$this->_iblockId,
						array(
							'FROM_XML_TMP' => 0
						)
					);
				}
			}

			foreach($goods as $good) {
				$props = array(
					'name' => trim((string)$good->name[0]['value']),
					'article' => trim((string)$good->articul[0]['value']),
					'preview_text' => trim((string)$good->text_pre[0]['value']),
					'detail_text' => trim((string)$good->text_det[0]['value']),
					'unit' => trim((string)$good->unit[0]['value']),
					'mpl' => trim((string)$good->kratnost[0]['value']),
					'sort' => trim((string)$good->sort[0]['value']),
					'price' => trim((string)$good->price[0]['value']),
					'cat' => trim((string)$good->catname[0]['value']),
					'discount_cat' => trim((string)$good->catdiscname[0]['value']),
					'brand' => trim((string)$good->brand[0]['value']),
					'new' => trim((string)$good->new[0]['value']),
					'shares' => trim((string)$good->shares[0]['value']),
				);

				$fields = array(
					'NAME' => $props['name'],
					'IBLOCK_ID' => $this->_iblockId,
					'ACTIVE' => 'Y',
					'PREVIEW_TEXT' => $props['preview_text'],
					'DETAIL_TEXT' => $props['detail_text'],
					'PREVIEW_TEXT_TYPE' => 'text',
					'DETAIL_TEXT_TYPE' => 'text',
					'SORT' => $props['sort'],
					'PROPERTY_VALUES' => array(
						'UNIT' => $props['unit'],
						'MPL' => $props['mpl'],
						'ARTICLE' => $props['article'],
						'FROM_XML_TMP' => 1,
						'CAT' => $this->getEnumVariantId(28, $props['cat']),
						'DISCOUNT_CAT' => $this->getEnumVariantId(29, $props['discount_cat']),
						'BRAND' => $this->getEnumVariantId(30, $props['brand']),
						'NEW' => $props['new']=='Y' ? 24 : false,
						'SHARES' => $props['shares']=='Y' ? 25 : false
					)
				);

				# проверяем, есть ли товар с таким артикулом
				$el = $ieObject->GetList(array(), array('IBLOCK_ID'=>$this->_iblockId, '=PROPERTY_ARTICLE'=>$props['article']));
				if($el->SelectedRowsCount()) {
					# апдейтим
					$el = $el->Fetch();

					if($ieObject->Update($el['ID'], $fields)) {
						$updated++;
					}
					else {
						$this->_errors[] = 'Ошибка при обновлении товара, артикул '.$props['article'].': '.$ieObject->LAST_ERROR;
					}

					# цена и количество
					$priceObject->SetBasePrice($el['ID'], round($props['price']*$props['mpl'], 2), 'RUB');
					$catObject->Update($el['ID'], array('QUANTITY'=>1, 'QUANTITY_TRACE'=>'N'));
				}
				else {
					# добавляем
					if($prId = $ieObject->Add($fields)) {
						$added++;

						# цена и количество
						$priceObject->SetBasePrice($prId, round($props['price']*$props['mpl'], 2), 'RUB');
						$catObject->Add(array('ID'=>$prId, 'QUANTITY'=>1, 'QUANTITY_TRACE'=>'N'));
					}
					else {
						$this->_errors[] = 'Ошибка при добавлении товара, артикул '.$props['article'].': '.$ieObject->LAST_ERROR;
					}
				}
			}
			# добавляем сопутствующие и альтернативные товары
			//$log = array();
			foreach($goods as $good) {
				$thisArticle = trim((string)$good->articul[0]['value']);
				$goodId = $this->getGoodId($thisArticle);
				$prValuesAdd = array();
				$prValuesAlt = array();

				$addGoods = $good->soputst;
				if(sizeof($addGoods)) {
					foreach($addGoods[0]->articul as $addArticle) {
						$article = trim((string)$addArticle['soputst_id']);
						$addId = $this->getGoodId($article);
						if(!$addId) {
							$this->_errors[] = 'Не найден товар с артикулом '.$article.', сопутствующий для товара с артикулом '.$thisArticle;
						}
						$prValuesAdd[] = array('VALUE'=>$addId);
					}
				}

				$altGoods = $good->alternative;
				if(sizeof($altGoods)) {
					foreach($altGoods[0]->articul as $altArticle) {
						$article = trim((string)$altArticle['alternative_id']);
						$altId = $this->getGoodId($article);
						if(!$altId) {
							$this->_errors[] = 'Не найден товар с артикулом '.$article.', альтернативный для товара с артикулом '.$thisArticle;
						}
						$prValuesAlt = array('VALUE'=>$altId);
					}
				}

				$ieObject->SetPropertyValuesEx(
					$goodId,
					$this->_iblockId,
					array(
						'ADD_GOODS' => $prValuesAdd,
						'ALT_GOODS' => $prValuesAlt
					)
				);
			}

			# деактивируем товары, у которых FROM_XML_TMP=0
			$rsElems = $ieObject->GetList(array(), array('IBLOCK_ID'=>$this->_iblockId, 'PROPERTY_FROM_XML_TMP'=>0));
			if($rsElems->SelectedRowsCount()) {
				while($arElem = $rsElems->Fetch()) {
					if($ieObject->Update($arElem['ID'], array('ACTIVE'=>'N'))) {
						$deleted++;
					}
					else {
						$this->_errors[] = 'Ошибка при деактивации товара, id '.$arElem['ID'].': '.$ieObject->LAST_ERROR;
					}
				}
			}
			$this->_successes[] = 'Товары: добавлено - '.$added.', обновлено - '.$updated.', деактивировано - '.$deleted;
		}

		if(@unlink($this->productsXmlPath)) {
			$this->_successes[] = 'Файл импорта товаров удалён';
		}
		else {
			$this->_errors[] = 'Не удалось удалить файл импорта товаров';
		}
		if(!sizeof($this->_errors)) {
			$this->_successes[] = 'Импорт успешно завершён';
		}
		$this->showMessages();

	}

	private function formatDiscount($discount) {
		$discount = trim((string)$discount);
		return str_replace(',', '.', $discount);
	}

	/**
	 * Импортирует юзеров
	 */
	public function importUsers() {
		# проверяем XML
		if(!$this->checkXml($this->usersXmlPath)) {
			$this->showMessages();
			return;
		}

		$xml = simplexml_load_file($this->usersXmlPath);
		$users = $xml->xpath('//Users/User');

		if(!sizeof($users)) {
			$this->errors[] = 'В файле выгрузки нет пользователей';
		}
		else {
			$userObject = new CUser;
			$added = $updated = 0;
			foreach($users as $user) {
				# поля xml
				$axaptaId = (string)$user->id[0];
				$userName = (string)$user->Name[0];
				$userSecondName = (string)$user->second_name[0];
				$userSurname = (string)$user->surname[0];
				$userGroup = (string)$user->group[0];
				$dealerId = (string)$user->dealer_id[0];
				$dealerName = (string)$user->dealer_name[0];
				$userEmail = (string)$user->email[0];
				$userAddEmails = explode(';', (string)$user->add_emails[0]);
				$userAddEmailsInvisible = explode(';', (string)$user->add_emails_invisible[0]);
				$userLogin = (string)$user->login[0];
				$userPass = (string)$user->pass[0];
				if(!$userPass) {
					$userPass = uniqid();
				}

				# проверяем обязательные поля
				if(!$userLogin || !$userEmail) {
					$this->_errors[] = 'У пользователя '.$axaptaId.' не задан логин или e-mail';
					continue;
				}


				# первым делом проверяем, есть ли уже юзер с таким id
				$rsUsers = $userObject->GetList($by='id', $order='asc', array(
					'UF_AXAPTA_ID' => $axaptaId
				));
				if($rsUsers->SelectedRowsCount()) {
					# юзер уже есть и группа, соответственно, тоже - апдейтим
					$arUser = $rsUsers->Fetch();
					$userObject->Update($arUser['ID'], array(
						'UF_DEALER_ID' => $dealerId,
						'UF_DEALER_NAME' => $dealerName,
						'UF_EMAILS_INV' => $userAddEmailsInvisible
					));
					$updated++;
				}
				else {
					# юзера ещё нет - создаём
					$userId = $userObject->Add(array(
						'LOGIN' => $userLogin,
						'EMAIL' => $userEmail,
						'PASSWORD' => $userPass,
						'CONFIRM_PASSWORD' => $userPass,
						'NAME' => $userName,
						'SECOND_NAME' => $userSecondName,
						'LAST_NAME' => $userSurname,
						'GROUP_ID' => array($userGroup),
						'UF_AXAPTA_ID' => $axaptaId,
						'UF_DEALER_ID' => $dealerId,
						'UF_DEALER_NAME' => $dealerName,
						'UF_EMAILS' => $userAddEmails,
						'UF_EMAILS_INV' => $userAddEmailsInvisible
					));
					if(!$userId) {
						$this->_errors[] = 'Не удалось добавить пользователя '.$axaptaId.': '.$userObject->LAST_ERROR;
					}
					else {
						$added++;
					}
				}
			}
			$this->_successes[] = 'Добавлено пользователей - '.$added;
			$this->_successes[] = 'Обновлено пользователей - '.$updated;

			if(@unlink($this->usersXmlPath)) {
				$this->_successes[] = 'Файл импорта пользователей удалён';
			}
			else {
				$this->_errors[] = 'Не удалось удалить файл импорта пользователей';
			}
			if(!sizeof($this->_errors)) {
				$this->successes[] = 'Импорт успешно завершён';
			}
		}
		$this->showMessages();
	}

	/**
	 * Импортирует скидки
	 */
	public function importDiscounts() {
		# проверяем XML
		if(!$this->checkXml($this->discountsXmlPath)) {
			$this->showMessages();
			return;
		}

		$xml = simplexml_load_file($this->discountsXmlPath);
		$discounts = $xml->xpath('//elements/elem');
		if(!sizeof($discounts)) {
			$this->_errors[] = 'В файле выгрузки не найдены скидки';
		}
		else {
			$this->clearDiscounts();
			$groupObject = new CGroup;
			$discountObject = new CCatalogDiscount;
			$userObject = new CUser;

			$by = 'id';
			$order = 'asc';
			$groupsAdded = 0;

			foreach($discounts as $discount) {
				$axaptaId = (string)$discount['id'];
				$groupDiscount = $this->formatDiscount($discount['value']);

				# первым делом добавляем группы пользователей
				$groupName ='Internal-'.$axaptaId;
				$rsGroups = $groupObject->GetList($by, $order, array('NAME'=>$groupName));
				$groupId = false;
				if(!$rsGroups->SelectedRowsCount()) {
					# группы нет - создаём
					if($groupId = $groupObject->Add(array('NAME'=>$groupName))) {
						$groupsAdded++;
					}
					else {
						$this->_errors[] = 'Не удалось добавить группу '.$groupName.': '.$groupObject->LAST_ERROR;
					}
				}
				else {
					$arGroup = $rsGroups->Fetch();
					$groupId = $arGroup['ID'];
				}

				if(!$groupId) continue;







				# проверяем, есть ли пользователь с таким axapta id, который ешё не состоит в группе
				$rsUsers = $userObject->GetList($by='id', $order='asc', array(
					'UF_AXAPTA_ID' => $axaptaId
				));

				if($rsUsers->SelectedRowsCount()) {
					$arUser = $rsUsers->Fetch();
					$arUserGroups = $userObject->GetUserGroup($arUser['ID']);
					if(!in_array($groupId, $arUserGroups)) {
						# пользователя нет в группе - добавляем
						$userObject->Update($arUser['ID'], array('UF_AXAPTA_ID'=>$axaptaId)); # апдейтим юзера, чтобы сработал обработчик
					}
				}







				# добавляем скидку группе пользователей (приоритет 1)
				$this->addDiscount($groupId, array(
					'NAME' => 'Пользователь: '.$axaptaId,
					'VALUE' => $groupDiscount,
					'PRIORITY' => 1,
					'CONDITIONS' => array(
						'CLASS_ID' => 'CondGroup',
						'DATA' => array(
							'All' => 'AND',
							'True' => 'True'
						),
						'CHILDREN' => array()
					),
					'UNPACK' => '((1 == 1))'
				));


				# скидки на категории скидок (приоритет 2)
				$discCats = $discount->cat_discount[0]->catdiscname;
				if(sizeof($discCats)) {
					foreach($discCats as $discCat) {
						$dcId = $this->getEnumVariantId(29, (string)$discCat['id']);
						$this->addDiscount($groupId, array(
							'NAME' => 'Пользователь: '.$axaptaId.'; категория скидки: '.(string)$discCat['id'],
							'VALUE' => $this->formatDiscount($discCat['value']),
							'PRIORITY' => 2,
							'CONDITIONS' => array(
								'CLASS_ID' => 'CondGroup',
								'DATA' => array(
									'All' => 'AND',
									'True' => 'True'
								),
								'CHILDREN' => array(
									array(
										'CLASS_ID' => 'CondIBProp:'.$this->_iblockId.':29',
										'DATA' => array(
											'logic' => 'Equal',
											'value' => $dcId
										)
									)
								)
							),
							'UNPACK' => '(($arProduct[\'IBLOCK_ID\'] == '.$this->_iblockId.' && isset($arProduct[\'PROPERTY_29_VALUE\']) && in_array('.$dcId.', $arProduct[\'PROPERTY_29_VALUE\'])))'
						));
					}
				}

				# скидки на бренды (приоритет 3)
				$brands = $discount->brends[0]->brend;
				if(sizeof($brands)) {
					foreach($brands as $brand) {
						$brId = $this->getEnumVariantId(30, (string)$brand['id']);
						$this->addDiscount($groupId, array(
							'NAME' => 'Пользователь: '.$axaptaId.'; бренд: '.(string)$brand['id'],
							'VALUE' => $this->formatDiscount($brand['value']),
							'PRIORITY' => 3,
							'CONDITIONS' => array(
								'CLASS_ID' => 'CondGroup',
								'DATA' => array(
									'All' => 'AND',
									'True' => 'True'
								),
								'CHILDREN' => array(
									array(
										'CLASS_ID' => 'CondIBProp:'.$this->_iblockId.':30',
										'DATA' => array(
											'logic' => 'Equal',
											'value' => $brId
										)
									)
								)
							),
							'UNPACK' => '(($arProduct[\'IBLOCK_ID\'] == '.$this->_iblockId.' && isset($arProduct[\'PROPERTY_30_VALUE\']) && in_array('.$brId.', $arProduct[\'PROPERTY_30_VALUE\'])))'
						));
					}
				}

				# скидки на категории (приоритет 4)
				$cats = $discount->categorys[0]->cat_name;
				if(sizeof($cats)) {
					foreach($cats as $cat) {
						$catId = $this->getEnumVariantId(28, (string)$cat['id']);
						$this->addDiscount($groupId, array(
							'NAME' => 'Пользователь: '.$axaptaId.'; категория: '.(string)$cat['id'],
							'VALUE' => $this->formatDiscount($cat['value']),
							'PRIORITY' => 4,
							'CONDITIONS' => array(
								'CLASS_ID' => 'CondGroup',
								'DATA' => array(
									'All' => 'AND',
									'True' => 'True'
								),
								'CHILDREN' => array(
									array(
										'CLASS_ID' => 'CondIBProp:'.$this->_iblockId.':28',
										'DATA' => array(
											'logic' => 'Equal',
											'value' => $catId
										)
									)
								)
							),
							'UNPACK' => '(($arProduct[\'IBLOCK_ID\'] == '.$this->_iblockId.' && isset($arProduct[\'PROPERTY_28_VALUE\']) && in_array('.$catId.', $arProduct[\'PROPERTY_28_VALUE\'])))'
						));
					}
				}

				# скидки на артикулы (приоритет 5)
				$articles = $discount->prices_percent[0]->price_p;
				if(sizeof($articles)) {
					foreach($articles as $article) {
						$arId = (string)$article['id'];
						$this->addDiscount($groupId, array(
							'NAME' => 'Пользователь: '.$axaptaId.'; артикул: '.(string)(string)$article['id'],
							'VALUE' => $this->formatDiscount($article['value']),
							'PRIORITY' => 5,
							'CONDITIONS' => array(
								'CLASS_ID' => 'CondGroup',
								'DATA' => array(
									'All' => 'AND',
									'True' => 'True'
								),
								'CHILDREN' => array(
									array(
										'CLASS_ID' => 'CondIBProp:'.$this->_iblockId.':1',
										'DATA' => array(
											'logic' => 'Equal',
											'value' => $arId
										)
									)
								)
							),
							'UNPACK' => '(($arProduct[\'IBLOCK_ID\'] == '.$this->_iblockId.' && isset($arProduct[\'PROPERTY_1_VALUE\']) && in_array("'.$arId.'", $arProduct[\'PROPERTY_1_VALUE\'])))'
						));
					}
				}

				# цены на артикулы (приоритет 6)
				$articles = $discount->prices[0]->price;
				if(sizeof($articles)) {
					foreach($articles as $article) {
						$arId = (string)$article['id'];
						$this->addDiscount($groupId, array(
							'NAME' => 'Пользователь: '.$axaptaId.'; артикул (цена): '.(string)(string)$article['id'],
							'VALUE' => $this->formatDiscount($article['value']),
							'PRIORITY' => 6,
							'VALUE_TYPE' => 'S',
							'CONDITIONS' => array(
								'CLASS_ID' => 'CondGroup',
								'DATA' => array(
									'All' => 'AND',
									'True' => 'True'
								),
								'CHILDREN' => array(
									array(
										'CLASS_ID' => 'CondIBProp:'.$this->_iblockId.':1',
										'DATA' => array(
											'logic' => 'Equal',
											'value' => $arId
										)
									)
								)
							),
							'UNPACK' => '(($arProduct[\'IBLOCK_ID\'] == '.$this->_iblockId.' && isset($arProduct[\'PROPERTY_1_VALUE\']) && in_array("'.$arId.'", $arProduct[\'PROPERTY_1_VALUE\'])))'
						));
					}
				}
			}

			$this->_successes[] = 'Группы пользователей: добавлено - '.$groupsAdded;
			$this->_successes[] = 'Скидки: добавлено - '.$this->_discountAdded;
		}

		# выгружаем акции
		$sharesAdded = 0;
		$sharesIblock = 4;
		$shares = $xml->xpath('//shares/share');
		$ieObject = $this->getBitrixObject('CIBlockElement');

		# удаляем все предыдущие акции
		$rsShares = $ieObject->GetList(array(), array('IBLOCK_ID'=>$sharesIblock));
		if($rsShares->SelectedRowsCount()) {
			while($arShare = $rsShares->Fetch()) {
				$ieObject->Delete($arShare['ID']);
			}
		}
		if(sizeof($shares)) {
			# добавляем новые
			foreach($shares as $share) {
				$arFields = array(
					'IBLOCK_ID' => $sharesIblock,
					'NAME' => trim((string)$share['id']),
					'CODE' => trim((string)$share['user_id']),
					'ACTIVE' => trim((string)$share['value'])
				);
				if($ieObject->Add($arFields)) {
					$sharesAdded++;
				}
				else {
					$this->_errors[] = 'Не удалось выгрузить акцию: '.$ieObject->LAST_ERROR;
				}
			}
			$this->_successes[] = 'Акции: добавлено - '.$sharesAdded;
		}

		if(@unlink($this->discountsXmlPath)) {
			$this->_successes[] = 'Файл импорта скидок удалён';
		}
		else {
			$this->_errors[] = 'Не удалось удалить файл импорта скидок';
		}
		if(!sizeof($this->_errors)) {
			$this->_successes[] = 'Импорт успешно завершён';
		}
		$this->showMessages();
	}

	/**
	 * Очищает скидки
	 */
	private function clearDiscounts() {
		global $DB;

		$sql = "DELETE FROM b_catalog_discount";
		$DB->Query($sql);

		$sql = "DELETE FROM b_catalog_discount_cond";
		$DB->Query($sql);

		$sql = "ALTER TABLE b_catalog_discount AUTO_INCREMENT=0";
		$DB->Query($sql);

		$sql = "ALTER TABLE b_catalog_discount_cond AUTO_INCREMENT=1";
		$DB->Query($sql);
	}

	/**
	 * Добавляет скидку
	 * @param $groupId id группы пользователя
	 * @param $arFields
	 */
	private function addDiscount($groupId, $arFields) {
		if(!$arFields['VALUE']) return;
		global $DB;

		$baseFields = array(
			'SITE_ID' => 's1',
			'ACTIVE' => 'Y',
			'RENEWAL' => 'N',
			'CURRENCY' => 'RUB',
			'LAST_DISCOUNT' => 'Y',
			'VERSION' => 2
		);
		$arFields = array_merge($baseFields, $arFields);
		if($arFields['CONDITIONS']) {
			$arFields['CONDITIONS'] = serialize($arFields['CONDITIONS']);
		}

		# вносим в основную таблицу
		foreach($arFields as $key=>$val) {
			if(is_string($val)) {
				$arFields[$key] = '"'.$DB->ForSql($val).'"';
			}
		}
		$id = $DB->Insert('b_catalog_discount', $arFields);

		# вносим в связующую таблицу
		$arFields = array(
			'DISCOUNT_ID' => $id,
			'USER_GROUP_ID' => $groupId,
			'PRICE_TYPE_ID' => 1
		);
		foreach($arFields as $key=>$val) {
			$arFields[$key] = $DB->ForSql($val);
		}
		$DB->Insert('b_catalog_discount_cond', $arFields);
		$this->_discountAdded++;
	}


	/**
	 * Проверка файла товаров
	 * @param $xmlPath путь до XML
	 * @return bool
	 */
	private function checkXml($xmlPath) {
		//$xmlPath = $_FILES[$name]['tmp_name'];
		# проверяем существования XML
		if(!file_exists($xmlPath)) {
			if(defined('AUTO_IMPORT')) {
				die('NOFILE');
			}
			$this->_errors[] = 'Не загружен XML';
		}

		# проверяем xml на валидность
		$testvalid = new DOMDocument();
		if(false === @$testvalid->load($xmlPath)) {
			$this->_errors[] = 'Некорректный XML';
		}

		return !(bool)sizeof($this->_errors);
	}
}

if(defined('AUTO_IMPORT')) {
	# автоматический импорт
	$baseXmlPath = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'import'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR;

	$ci = new CatImport;
	$ci->productsXmlPath = $baseXmlPath.'products.xml';
	$ci->discountsXmlPath = $baseXmlPath.'discounts.xml';
	$ci->usersXmlPath = $baseXmlPath.'users.xml';

	if(defined('AUTO_IMPORT_PRODUCTS')) {
		$ci->importGoods();
	}
	elseif(defined('AUTO_IMPORT_DISCOUNTS')) {
		$ci->importDiscounts();
	}
	elseif(defined('AUTO_IMPORT_USERS')) {
		$ci->importUsers();
	}
	BXClearCache(true);
}
else {
	# импорт вручную через форму
	if(isset($_POST['xmlSubmit'])) {
		$ci = new CatImport;
		if(isset($_FILES['goodsXml'])) {
			# выгружаем товары
			$ci->productsXmlPath = @$_FILES['goodsXml']['tmp_name'];
			$ci->importGoods();
		}
		elseif(isset($_FILES['discountXml'])) {
			# выгружаем скидки
			$ci->discountsXmlPath = @$_FILES['discountXml']['tmp_name'];
			$ci->importDiscounts();
		}
		elseif(isset($_FILES['usersXml'])) {
			# выгружаем пользователей
			$ci->usersXmlPath = @$_FILES['usersXml']['tmp_name'];
			$ci->importUsers();
		}
		BXClearCache(true);
	}
}



$this->IncludeComponentTemplate();