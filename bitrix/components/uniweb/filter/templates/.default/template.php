<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!function_exists('getPropSelect')) {
	function getPropSelect($propId, $selectName, $optionText='любой') {
		global $APPLICATION;
		$output = '';
		$rsVariants = CIBlockProperty::GetPropertyEnum($propId);

		# убираем "любая" из селекта категорий
		if($propId == 28) {
			$options = array();
		}
		else {
			$options = array(array('val'=>'', 'text'=>$optionText));
		}

		if($rsVariants->SelectedRowsCount()) {
			while($arVariant = $rsVariants->Fetch()) {
				$option = array('val'=>$arVariant['ID'], 'text'=>$arVariant['VALUE'], 'xmlid'=>$arVariant['XML_ID']);
				if(isset($_GET[$selectName]) && $_GET[$selectName]==$arVariant['ID']) {
					$option['selected'] = true;
				}
				$options[] = $option;
			}
		}
		$arJsOptions = array();
		foreach($options as $option) {
			$output .= '<option'.(array_key_exists('selected', $option) ? ' selected="selected"' : '').' value="'.$option['val'].'" data-xmlid="'.$option['xmlid'].'">'.$option['text'].'</option>';
			$arJsOptions[$option['xmlid']] = $option;
		}
		if($propId == 28) {
			# публикуем js-объект с option'ами селекта
			print_r($arJsOptions);
			$APPLICATION->AddHeadString('<script>var catOptions = '.json_encode($arJsOptions).';</script>');
		}
		return $output;
	}
}

$name = (isset($_GET['name']) && trim($_GET['name'])) ? htmlspecialchars(trim($_GET['name'])) : '';
$priceMinValue = (isset($_GET['priceMin']) && (int)$_GET['priceMin']) ? (int)$_GET['priceMin'] : '';
$priceMaxValue = (isset($_GET['priceMax']) && (int)$_GET['priceMax']) ? (int)$_GET['priceMax'] : '';
$newChecked = (isset($_GET['new']) && $_GET['new']=='Y') ? ' checked="chedcked"' : '';
$sharesChecked = (isset($_GET['shares']) && $_GET['shares']=='Y') ? ' checked="chedcked"' : '';
$orderedChecked = (isset($_GET['ordered']) && $_GET['ordered']=='Y') ? ' checked="chedcked"' : '';

#print_r($arResult);
?>

<form class="filter clearfix">
	<input type="hidden" name="setFilter" value="Y"/>
	<!--div class="filterLeft">
		<label for="priceMin">Цена от</label>
		<input type="text" name="priceMin" id="priceMin" value="<?=$priceMinValue?>"/>
		<label for="priceMax">до</label>
		<input type="text" name="priceMax" id="priceMax" value="<?=$priceMaxValue?>"/>
		<span>руб.</span>
	</div-->

	<div class="filterLeft">
		<label for="name">Название:</label>
		<input type="text" name="name" id="name" value="<?=$name?>"/>
	</div>

	<div class="filterLeft">
		<label for="brand">Бренд</label>
		<select name="brand" id="brand" class="select">
			<?=getPropSelect(30, 'brand')?>
		</select>
	</div>

	<div class="filterLeft">
		<label for="cat">Категория</label>
		<select name="cat" id="cat" class="select">
			<?=getPropSelect(28, 'cat', 'любая')?>
		</select>
	</div>

	<?php
	# если пользователь авторизован - выводим фильтр по ранее заказанным товарам
	$class = $USER->IsAuthorized() ? ' filterCheckboxes' : '';
	?>
	<div class="filterLeft tar<?=$class?>">
		<label for="shares">Акция</label>
		<input<?=$sharesChecked?> type="checkbox" name="shares" id="shares" class="checkbox" value="Y"/>
		<label for="shares"></label>
		&nbsp;
		<label for="new">Новинка</label>
		<input<?=$newChecked?> type="checkbox" name="new" id="new" class="checkbox" value="Y"/>
		<label for="new"></label><br>

		<?if($USER->IsAuthorized()):?>
		<label for="shares">Заказанные ранее</label>
		<input<?=$orderedChecked?> type="checkbox" name="ordered" id="ordered" class="checkbox" value="Y"/>
		<label for="ordered"></label>
		<?endif;?>
	</div>
	<div class="filterRight">
		<button class="orangeButton showButton">Показать</button>
	</div>
</form>