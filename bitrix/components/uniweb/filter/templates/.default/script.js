$(function() {
	// связи брендов и категорий
	if(window.catRelations) {
		// при первом заполнении учитывать "selected"
		var firstRender = true;

		// заполняет селект категорий опшенами
		function fillOptions(brand) {
			if(!window.catOptions) return;
			var selected = 0;

			// удаляем все опшены
			$('select#cat option').remove();
			$('<option value="">любая</option>').appendTo('select#cat');

			if(typeof brand != 'undefined') {
				for(var i=0; i<catRelations[brand].length; i++) {
					var catId = catRelations[brand][i];
					var optionObj = catOptions[catId];
					var option = '<option value="'+optionObj.val+'" data-xmlid="'+optionObj.xmlid+'">'+optionObj.text+'</option>';

					if(optionObj.selected) {
						selected = optionObj.val;
					}

					// добавляем в селект
					$(option).appendTo('select#cat');
				}
			}
			else {
				for(optId in catOptions) {
					var optionObj = catOptions[optId];
					var option = '<option value="'+optionObj.val+'" data-xmlid="'+optionObj.xmlid+'">'+optionObj.text+'</option>';

					if(optionObj.selected) {
						selected = optionObj.val;
					}

					// добавляем в селект
					$(option).appendTo('select#cat');
				}
			}
			// устанавливаем selected
			if(firstRender && selected) {
				$('select#cat').val(selected);
				firstRender = false;
			}
			$('select#cat').trigger('change');
		}
		$('select#brand').on('change', function() {
			var val = $(this).val();
			var selectedOption = $(this).find('option[value="'+val+'"]');
			var xmlid = selectedOption.data('xmlid');

			if(!xmlid || !catRelations[xmlid]) {
				// возвращаем полный селект
				fillOptions();
			}
			else {
				// возвращаем селект со связанными вариантами
				fillOptions(xmlid);
			}
		}).trigger('change');
	}
});