<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => 'Фильтр каталога',
	"DESCRIPTION" => 'Фильтр на странице каталога',
	"ICON" => "/images/sections_top.gif",
	"CACHE_PATH" => "Y",
	"SORT" => 110,
	"PATH" => array(
		"ID" => "Uniweb"
	),
);

?>