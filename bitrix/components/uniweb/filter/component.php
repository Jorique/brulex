<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule('iblock');
$arResult = array();

if(isset($_GET['setFilter']) && $_GET['setFilter']=='Y') {
	$catFilter = array();
	$GLOBALS['catFilter'] = &$catFilter;

	# название
	if(isset($_GET['name']) && trim($_GET['name'])) {
		$catFilter['NAME'] = '%'.trim($_GET['name']).'%';
	}

	# цены
	if(isset($_GET['priceMin']) && (int)$_GET['priceMin']) {
		$catFilter['>=CATALOG_PRICE_1'] = (int)$_GET['priceMin'];
	}
	if(isset($_GET['priceMax']) && (int)$_GET['priceMax']) {
		$catFilter['<=CATALOG_PRICE_1'] = (int)$_GET['priceMax'];
	}

	# категория
	if(isset($_GET['cat']) && (int)$_GET['cat']) {
		$catFilter['PROPERTY_CAT'] = (int)$_GET['cat'];
	}

	# бренд
	if(isset($_GET['brand']) && (int)$_GET['brand']) {
		$catFilter['PROPERTY_BRAND'] = (int)$_GET['brand'];
	}

	# новинка
	if(isset($_GET['new']) && $_GET['new']=='Y') {
		$catFilter['PROPERTY_NEW_VALUE'] = 'Y';
	}

	# акция
	if(isset($_GET['shares']) && $_GET['shares']=='Y') {
		$catFilter['PROPERTY_SHARES_VALUE'] = 'Y';
	}

	# уже заказанные товары
	if($USER->IsAuthorized() && isset($_GET['ordered']) && $_GET['ordered']=='Y') {
		# получаем список заказов пользователя
		CModule::IncludeModule('sale');
		$orderObject = new CSaleOrder;
		$basketObject = new CSaleBasket;
		$arIds = array();

		$rsOrders = $orderObject->GetList(
			array('ID' => 'DESC'),
			array('USER_ID' => $USER->GetID()),
			false,
			false,
			array('ID')
		);
		if($rsOrders->SelectedRowsCount()) {
			while($arOrder = $rsOrders->Fetch()) {
				# получаем товары заказа
				$rsGoods = $basketObject->GetList(
					array(),
					array('ORDER_ID' => $arOrder['ID']),
					false,
					false,
					array(
						'ID',
						'PRODUCT_ID'
					)
				);
				if($rsGoods->SelectedRowsCount()) {

					while($arGood = $rsGoods->Fetch()) {
						if(!in_array($arGood['PRODUCT_ID'], $arIds)) {
							$arIds[] = $arGood['PRODUCT_ID'];
						}
					}
				}
			}
		}
		if(!empty($arIds)) {
			$arLogic = array('LOGIC' => 'OR');
			foreach($arIds as $id) {
				$arLogic[] = array('ID' => $id);
			}
			$catFilter[] = $arLogic;
		}
		else {
			$catFilter['ID'] = -1; # чтоб не искалось
		}
	}
}

# связи категорий и брендов
$arBrands = COption::GetOptionString('main', 'cat_relations');
if($arBrands) {
	$APPLICATION->AddHeadString('<script>var catRelations = '.$arBrands.';</script>');
}

$this->IncludeComponentTemplate();