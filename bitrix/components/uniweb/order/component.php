<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule('sale');

# возвращает адрес сервера
if(!function_exists('getSiteUrl')) {
	function getSiteUrl() {
		$httpsPort = 443;
		$siteUrl= ((isset ($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') || $_SERVER['SERVER_PORT'] == $httpsPort) ? 'https://' : 'http://';
		$siteUrl .= $_SERVER['HTTP_HOST'];
		if ($_SERVER['SERVER_PORT'] != 80) {
			$siteUrl= str_replace(':' . $_SERVER['SERVER_PORT'], '', $siteUrl); # удаляем порт из HTTP_HOST
		}
		$siteUrl .= ($_SERVER['SERVER_PORT'] == 80 || (isset ($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') || $_SERVER['SERVER_PORT'] == $httpsPort) ? '' : ':' . $_SERVER['SERVER_PORT'];
		return $siteUrl.'/';
	}
}

# добавление свойства заказа
if(!function_exists('addOrderProperty')) {
	function addOrderProperty($code, $value, $orderId, $personTypeId) {
		if (!strlen($code)) {
			return false;
		}
		if ($arProp = CSaleOrderProps::GetList(array(), array('CODE'=>$code, 'PERSON_TYPE_ID'=>$personTypeId))->Fetch()) {
			return CSaleOrderPropsValue::Add(array(
				'NAME' => $arProp['NAME'],
				'CODE' => $arProp['CODE'],
				'ORDER_PROPS_ID' => $arProp['ID'],
				'ORDER_ID' => $orderId,
				'VALUE' => $value,
			));
		}
	}
}

# получаем e-mail'ы менеджеров
if(!function_exists('getManagersEmails')) {
	function getManagersEmails() {
		$emails = array();
		$rsManagers = CUser::GetList($by='id', $order='asc', array('GROUPS_ID'=>array(1157), 'ACTIVE'=>'Y'));
		if($rsManagers->SelectedRowsCount()) {
			while($arManager = $rsManagers->Fetch()) {
				$emails[] = $arManager['EMAIL'];
			}
		}
		return implode(',', $emails);
	}
}


$arResult = array();
$arResult['ERRORS'] = array();
$arResult['ORDER_ADDED'] = false;
$arResult['CART_EMPTY'] = true;

# получаем товары текущей корзины
$rsGoods = CSaleBasket::GetList(
	array(),
	array(
		'FUSER_ID' => CSaleBasket::GetBasketUserID(),
		'ORDER_ID' => false
	),
	false,
	false,
	array(
		'PRODUCT_ID',
		'ORDER_ID',
		'NAME',
		'PRICE',
		'QUANTITY',
		'FUSER_ID'
	)
);
if($rsGoods->SelectedRowsCount()) {
	$arResult['CART_EMPTY'] = false;
}

# вычисляем тип пользователя - дилер или регистрированнный
$groups = CUser::GetUserGroup($USER->GetID());
if(in_array(8, $groups)) {
	$personType = 4;
}
else {
	$personType = 3;
}

# вычисляем, на какую почту отправлять уведомления
$userFields = CUser::GetByID($USER->GetID())->Fetch();
$axaptaId = trim($userFields['UF_AXAPTA_ID']);
$dealerId = trim($userFields['UF_DEALER_ID']);
if($personType == 4) {
	# покупатель - дилер, отправляем на почту менеджеров сайта
	$recipientEmail = getManagersEmails();
}
elseif($dealerId) {
	# у покупателя проставлен id дилера, отправляем на почту дилера
	$rsDealer = CUser::GetList($by='id', $order='asc', array('UF_DEALER_ID'=>$dealerId, 'GROUPS_ID'=>array(8)));
	if($rsDealer->SelectedRowsCount()) {
		$arDealer = $rsDealer->Fetch();
		$recipientEmail = $arDealer['EMAIL'];
	}
	else {
		# дилер не найден, отправляем менеджерам
		$recipientEmail = getManagersEmails();
	}
}
else {
	# покупатель без дилера - отправляем менеджерам сайта
	$recipientEmail = getManagersEmails();
}

# дополняем список адресов e-mail'ами заказчика
$addEmails = array();
$addEmails[] = $USER->GetEmail();
if(is_array($userFields['UF_EMAILS']) && sizeof($userFields['UF_EMAILS'])) {
	$addEmails = array_merge($addEmails, $userFields['UF_EMAILS']);
}
if(is_array($userFields['UF_EMAILS_INV']) && sizeof($userFields['UF_EMAILS_INV'])) {
	$addEmails = array_merge($addEmails, $userFields['UF_EMAILS_INV']);
}
$addEmails = array_unique($addEmails);

if($recipientEmail && sizeof($addEmails)) {
	$recipientEmail .= ', '.implode(', ', $addEmails);
}
elseif($addEmails) {
	$recipientEmail = implode(', ', $addEmails);
}

/*echo '<pre>';
print_r($recipientEmail);
echo '</pre>';*/


if(isset($_POST['orderSubmit']) && !$arResult['CART_EMPTY']) {
	# форму отправили, товары в корзине есть
	$fio = isset($_POST['fio']) ? trim($_POST['fio']) : '';
	$phone = isset($_POST['phone']) ? trim($_POST['phone']) : '';
	$email = isset($_POST['email']) ? trim($_POST['email']) : '';
	$comment = isset($_POST['comment']) ? trim($_POST['comment']) : '';

	if(!$fio) {
		$arResult['ERRORS'][] = 'Не заполнено поле Ф. И. О.';
	}
	if(!$phone) {
		$arResult['ERRORS'][] = 'Укажите ваш телефон';
	}
	if(!$email) {
		$arResult['ERRORS'][] = 'Укажите ваш e-mail';
	}
	elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$arResult['ERRORS'][] = 'Некорректный e-mail';
	}

	if(!sizeof($arResult['ERRORS'])) {
		# ошибок нет - формируем заказ
		$sum = 0;
		$orderFileGoods = array();
		$siteUrl = getSiteUrl();

		$orderDetail = '<table border="1" bordercolor="grey" cellpadding="10" cellspacing="0"><tr><th>Артикул</th><th>Наименование</th><th>Упаковка</th><th>Кол-во упаковок</th><th>Стоимость</th></tr>';
		while($arGoods = $rsGoods->Fetch()) {
			# забираем кратность товара и единицы измерения
			$arProduct = CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_ID' => 3,
					'ID' => $arGoods['PRODUCT_ID']
				),
				false,
				false,
				array(
					'ID',
					'DETAIL_PAGE_URL',
					'PROPERTY_MPL',
					'PROPERTY_UNIT',
					'PROPERTY_ARTICLE'
				)
			)->GetNextElement()->GetFields();

			$thisSum = $arGoods['PRICE']*$arGoods['QUANTITY'];
			$sum += $thisSum;
			$orderDetail .= '<tr>';
			$orderDetail .= '<td>'.$arProduct['PROPERTY_ARTICLE_VALUE'].'</td>';
			$orderDetail .= '<td><a href="'.rtrim($siteUrl, '/').$arProduct['DETAIL_PAGE_URL'].'">'.$arGoods['NAME'].'</a></td>';
			$orderDetail .= '<td>'.$arProduct['PROPERTY_MPL_VALUE'].' '.$arProduct['PROPERTY_UNIT_VALUE'].'</td>';
			$orderDetail .= '<td>'.(int)$arGoods['QUANTITY'].'</td>';
			$orderDetail .= '<td>'.FormatCurrency($thisSum, 'RUB').'</td>';
			$orderDetail .= '</tr>';

			$orderFileGoods[] = '"'.$arProduct['PROPERTY_ARTICLE_VALUE'].'","'.(int)$arGoods['QUANTITY'].'","'.round($thisSum,2).'"';
		}
		$sum = round($sum, 2);
		$orderDetail .= '</table>';
		$arResult['ORDER_PRICE'] = $sum;


		$orderId = CSaleOrder::Add(array(
			'LID' => SITE_ID,
			'PERSON_TYPE_ID' => $personType,
			'PRICE' => $sum,
			'CURRENCY' => 'RUB',
			'USER_ID' => $USER->GetID(),
			'PAY_SYSTEM_ID' => 1,
			'DELIVERY_ID' => 2,
			'USER_DESCRIPTION' => $comment
		));
		if(!$orderId) {
			$ex = $APPLICATION->GetException();
			$arResult['ERRORS'][] = $ex->GetString();
		}
		else {
			# добавляем товары к заказу
			CSaleBasket::OrderBasket($orderId);

			# добавляем свойства к заказу
			addOrderProperty('FIO', $fio, $orderId, $personType);
			addOrderProperty('PHONE', $phone, $orderId, $personType);
			addOrderProperty('EMAIL', $email, $orderId, $personType);

			# инициируем почтовое событие
			CEvent::Send('UNIWEB_NEW_ORDER', 's1', array(
				'ORDER_ID' => $orderId,
				'ORDER_DETAIL' => $orderDetail,
				'ORDER_PRICE' => FormatCurrency($sum, 'RUB'),
				'CLIENT_FIO' => $fio,
				'CLIENT_PHONE' => $phone,
				'CLIENT_EMAIL' => $email,
				'RECIPIENT_EMAIL' => $recipientEmail,
				'AXAPTA_ID' => $axaptaId,
				'DEALER_ID' => $dealerId,
				'COMMENT' => $comment
			), 'N');

			# генерируем файл с заказом
			$filePath = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'orderfiles'.DIRECTORY_SEPARATOR;
			$fileEmails = implode(';', preg_split('#\s*,\s*#', $recipientEmail));
			$orderFile = '"'.$axaptaId.'","'.$fileEmails.'","'.$dealerId.'"'."\r\n".implode("\r\n", $orderFileGoods);
			file_put_contents($filePath.$orderId.'.ord', $orderFile);

			$arResult['ORDER_ADDED'] = true;
			$arResult['ORDER_ID'] = $orderId;
			define('ORDER_ADDED', true); # для компонента корзины
		}
	}
}

$this->IncludeComponentTemplate();