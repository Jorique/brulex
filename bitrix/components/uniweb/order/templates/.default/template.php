<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arUser = CUser::GetById($USER->GetID())->Fetch();
$name= $arUser['NAME'];
$secondName = $arUser['SECOND_NAME'];
$surname = $arUser['LAST_NAME'];
$arName = array();

if($name) {
	$arName[] = $name;
}
if($name && $secondName) {
	$arName[] = $secondName;
}
if($surname) {
	$arName[] = $surname;
}
$name = isset($_POST['fio']) ? htmlspecialchars($_POST['fio']) : implode(' ', $arName);
$phone = isset($_POST['phone']) ? htmlspecialchars($_POST['phone']) : $arUser['PERSONAL_PHONE'];
$email = isset($_POST['email']) ? htmlspecialchars($_POST['email']) : $arUser['EMAIL'];

#echo '<pre>'.print_r($arResult,1).'</pre>';
if($arResult['CART_EMPTY']) {
	return;
}
$this->SetViewTarget("order");
?>



<form method="post" class="orderForm">
	<?php
	if(sizeof($arResult['ERRORS'])) {
		echo '<ul class="errors">';
		foreach($arResult['ERRORS'] as $error) {
			echo '<li>'.$error.'</li>';
		}
		echo '</ul>';
	}
	?>

	<?if($arResult['ORDER_ADDED']):?>

		<p><strong>Заказ сформирован!</strong></p>
		<p>Ваш заказ <strong>№ <?=$arResult['ORDER_ID']?></strong> от <strong><?=date('d.m.Y')?></strong> принят.<br>
		Стоимость заказа: <strong><?=FormatCurrency($arResult['ORDER_PRICE'], 'RUB')?></strong></p>
		<script type="text/javascript">
			$(function() {
				$('.cartPhCount').html(0);
				$('.cartPhPricePrint').html('0 руб.');
				$('.cartPhPlural').html('Товаров');
			});
		</script>
		<?return?>
	<?endif;?>

	<div class="greyBlock">
		<div class="fieldBlock">
			<label for="orderFio">Ф. И. О. <span class="required">*</span></label>
			<input type="text" name="fio" id="orderFio" value="<?=$name?>"/>
		</div>
		<div class="fieldBlock">
			<label for="orderPhone">Телефон <span class="required">*</span></label>
			<input type="text" name="phone" id="orderPhone" value="<?=$phone?>"/>
		</div>
		<div class="fieldBlock">
			<label for="orderEmail">E-mail <span class="required">*</span></label>
			<input type="text" name="email" id="orderEmail" value="<?=$email?>"/>
		</div>
		<br>
		<div class="fieldBlock orderComment">
			<label for="orderComment">Дополнительная информация по заказу</label>
			<textarea name="comment" id="orderComment"></textarea>
		</div>
	</div>
	<br>
	<button name="orderSubmit" class="orangeButton">Оформить заказ</button>
</form>

<?$this->EndViewTarget("order");?>