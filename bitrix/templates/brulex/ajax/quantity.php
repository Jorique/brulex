<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!isset($_POST['id']) || !isset($_POST['quantity'])) die;

CModule::IncludeModule('sale');
//CModule::IncludeModule('catalog');
$id = (int)$_POST['id'];
$quantity = (int)$_POST['quantity'];
CSaleBasket::Update($id, array('QUANTITY'=>$quantity));

$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.line",
	"json",
	Array(),
	false,
	array(
		'HIDE_ICONS' => 'Y'
	)
);