<?php

# для формы регистрации
if(isset($_REQUEST['WEB_FORM_ID'])) {
	$_SERVER["HTTP_X_REQUESTED_WITH"] = "XMLHttpRequest";
	if($_REQUEST['WEB_FORM_ID']==1) {
		$_REQUEST['act'] = 'register_request';
	}
	elseif($_REQUEST['WEB_FORM_ID']==3) {
		$_REQUEST['act'] = 'call_request';
	}
}

if($_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest"){
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
}

# для формы регистрации
if(isset($_REQUEST['WEB_FORM_ID']) && $_REQUEST['WEB_FORM_ID']==1) {
	$_REQUEST['act'] = 'register_request';
}
elseif(isset($_REQUEST['WEB_FORM_ID']) && $_REQUEST['WEB_FORM_ID']==3) {
	$_REQUEST['act'] = 'call_request';
}

$dir = realpath(dirname(__FILE__));
$file = str_replace('..', '', $_REQUEST['act'].'.php');
$path = $dir.DIRECTORY_SEPARATOR.$file;
$path = preg_replace(array('/(\.+\/)/'), array(''), $path);
//$path = preg_replace(array('/^(\/)?|(\/)+/', '/(\.+\/)/'), array('/', ''), $path);
$path = str_replace('/', DIRECTORY_SEPARATOR, $path);

if(isset($_REQUEST['act']) && file_exists($path)) {
	require $path;
}

if($_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest"){
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
}
?>