<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(!isset($_GET['id'])) die;

CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');
$id = (int)$_GET['id'];
$ieObject = new CIBlockElement;

$rsProduct = $ieObject->GetList(
	array(),
	array(
		'IBLOCK_ID' => 3,
		'ID' => $id
	),
	false,
	false,
	array(
		'ID',
		'DETAIL_PAGE_URL',
		'NAME',
		'DETAIL_PICTURE',
		'DETAIL_TEXT',
		'PROPERTY_BRAND',
		'PROPERTY_MPL',
		'PROPERTY_UNIT',
		'PROPERTY_ARTICLE'
	)
);
if($rsProduct->SelectedRowsCount()) {
	$rsProduct = $rsProduct->GetNextElement();
	$arProduct = $rsProduct->GetFields();

	$folderImage = getImageFromFolder($arProduct['PROPERTY_ARTICLE_VALUE']);
	if($arProduct['DETAIL_PICTURE']) {
		$img = CFile::GetPath($arProduct['DETAIL_PICTURE']);
	}
	elseif($folderImage) {
		$img = $folderImage;
	}
	else {
		$img = SITE_TEMPLATE_PATH.'/images/no_photo.jpg';
	}

	$arPrice = CCatalogProduct::GetOptimalPrice($arProduct['ID'], 1, $USER->GetUserGroupArray());
	$price = $arPrice['PRICE']['PRICE'];
	$discountPrice = $arPrice['DISCOUNT_PRICE'];
	$mpl = (float)$arProduct['PROPERTY_MPL_VALUE'];
	if($mpl>0) {
		$price = round($price/$mpl, 2);
		$discountPrice = round($discountPrice/$mpl, 2);
	}
	?>

	<div class="ajaxProduct clearfix">
		<div class="ajaxProductLeft"><img src="<?=$img?>" alt="<?=$arProduct['NAME']?>"/></div>
		<div class="ajaxProductRight content">
			<h1><?=$arProduct['NAME']?></h1>

			<p><strong>Цена:</strong>
				<?if($USER->IsAuthorized()):?>
					<?if($discountPrice < $price):?>
						<s><?=FormatCurrency($price, 'RUB')?></s>
					<?endif;?>
					<?=FormatCurrency($discountPrice, 'RUB')?>/<?=$arProduct['PROPERTY_UNIT_VALUE']?>
				<?else:?>
					Авторизуйтесь
				<?endif;?>
			</p>
			<p><strong>Упаковка:</strong> <?=$arProduct['PROPERTY_MPL_VALUE']?> <?=$arProduct['PROPERTY_UNIT_VALUE']?></p>
			<p><strong>Бренд:</strong> <?=$arProduct['PROPERTY_BRAND_VALUE']?></p>
			<?if($arProduct['DETAIL_TEXT']):?>
				<p><strong>Описание</strong> <?=$arProduct['DETAIL_TEXT']?></p>
			<?endif;?>
			<p><a href="<?=$arProduct['DETAIL_PAGE_URL']?>">Показать полное описание</a></p>

			<?if((float)$price && $USER->IsAuthorized()):?>
				<a data-id="<?=$arProduct['ID']?>" data-quantity="1" href="/catalog/?action=ADD2BASKET&id=<?=$arProduct['ID']?>" class="orangeButton toCart toCartBig"><span></span>Добавить в корзину</a>
			<?else:?>
				<a class="orangeButton toCart toCartBig blocked" href="javascript:void(0);"><span></span>Добавить в корзину</a>
			<?endif;?>
		</div>
	</div>

	<?
}
else {
	echo 'Товар не найден';
}