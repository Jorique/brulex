<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<header class="header blockWrapper">
	<div class="innerHeader">
		<div class="headerLogo">
			<a class="selfLink" href="/" title="<?=$site['NAME']?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="<?=$site['NAME']?>" /></a>
			<form action="/search/">
				<input type="submit" />
				<input class="iconInput" type="text" name="q" placeholder="Поиск по сайту" />
			</form>
		</div>
		<div class="headerPhones">
			<div class="headerPhone">
				<span>
				<?$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH.'/inc_areas/code1.php',
					array(),
					array(
						'NAME' => 'код телефона',
						'MODE'=>'text'
					)
				)?>
				</span>
				<div>
				<?$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH.'/inc_areas/phone1.php',
					array(),
					array(
						'NAME' => 'телефон',
						'MODE'=>'text'
					)
				)?>
				</div>
			</div>
			<div class="headerFax">
				<span>
				<?$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH.'/inc_areas/code2.php',
					array(),
					array(
						'NAME' => 'код телефона',
						'MODE'=>'text'
					)
				)?>
				</span>
				<div>
				<?$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH.'/inc_areas/phone2.php',
					array(),
					array(
						'NAME' => 'телефон',
						'MODE'=>'text'
					)
				)?>
				</div>
			</div>
			<div class="headerMail">
				<div>
				<a href="mailto:<? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/inc_areas/email.php' ?>">
					<?$APPLICATION->IncludeFile(
						SITE_TEMPLATE_PATH.'/inc_areas/email.php',
						array(),
						array(
							'NAME' => 'e-mail',
							'MODE'=>'text'
						)
					)?>
				</a>
				</div>
			</div>


			<div class="headerCall">
				<a href="javascript:void(0);">Заказать обратный звонок</a>
				<? require 'call-request.inc.php' ?>
			</div>

		</div>
		<div class="headerCart">
			<? require 'top-cart.inc.php' ?>
			<div class="headerCartShadow"></div>
		</div>
		<div class="headerAuth">
			<?if($USER->IsAuthorized()):?>
				<div class="headerAuthInner">
					<div class="userName"><?=$USER->GetFormattedName()?></div>
					<a class="profileLink" href="/personal/">Профиль</a> / <a href="/?logout=yes">Выйти</a>
				</div>
			<?else:?>
			<form action="/login/" method="post" class="headerAuthInner">
				<input type="hidden" value="Y" name="AUTH_FORM">
				<input type="hidden" value="AUTH" name="TYPE">
				<input type="hidden" value="<?=$APPLICATION->GetCurUri()?>" name="backurl">

				<input class="iconInput headerAuthLogin" type="text" name="USER_LOGIN" placeholder="Логин" />
				<input class="iconInput headerAuthPass" type="password" name="USER_PASSWORD" placeholder="Пароль" /><br>
				<button class="link" name="Login">Вход</button> / <a class="registerLink" href="/login/?register=yes">Регистрация</a>
			</form>
			<? require 'register-request.inc.php' ?>
			<?endif;?>
			<div class="headerAuthShadow"></div>
		</div>
	</div>
	<? require 'inc/top-menu.inc.php' ?>
</header>