<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule('iblock');
$ieObject = new CIBlockElement;
$rsShares = $ieObject->GetList(
	array(
		'DATE_ACTIVE_FROM' => 'DESC',
		'SORT' => 'ASC',
		'ID' => 'DESC'
	),
	array(
		'IBLOCK_ID' => 5,
		'ACTIVE' => 'Y',
		'PROPERTY_SHOW_IN_SLIDER_VALUE' => 'Y',
		'!PROPERTY_SLIDER_IMAGE' => false
	),
	false,
	false,
	array(
		'ID',
		'DETAIL_PAGE_URL',
		'NAME',
		'PREVIEW_TEXT',
		'PROPERTY_SLIDER_IMAGE'
	)
);
$count = $rsShares->SelectedRowsCount();
if($count) {
	?>

	<div class="blockWrapper">
		<section class="scrollable<?=$count>1 ? ' indexSlider' : ''?>">
			<ul class="scrollableItems">
			<?while($arShare = $rsShares->GetNextElement()):?>
				<? $arShare = $arShare->GetFields(); ?>
				<li>
					<a href="<?=$arShare['DETAIL_PAGE_URL']?>" title="<?=$arShare['NAME']?>">
						<img src="<?=CFile::GetPath($arShare['PROPERTY_SLIDER_IMAGE_VALUE'])?>" alt="<?=$arShare['NAME']?>"/>

						<div class="actionText">
							<div class="actionHeader"><?=$arShare['NAME']?></div>
							<div class="actionDesc"><?=$arShare['PREVIEW_TEXT']?></div>
						</div>
					</a>
				</li>
			<?endwhile;?>
			</ul>
			<?if($count>1):?>
				<span class="indexSliderLeft"></span>
				<span class="indexSliderRight"></span>
				<div class="indexSliderNav navi"></div>
			<?endif;?>
		</section>
	</div>

	<?
}
?>