<?php
# массив информации по сайту
$site = CSite::GetById(SITE_ID)->Fetch();

# проверяем на главную страницу
if($_SERVER['SCRIPT_FILENAME']==rtrim($_SERVER['DOCUMENT_ROOT'], '/').'/index.php') define('DI_MAIN_PAGE', true);

# провеяем на детальную страницу товара
if(preg_match('#^/catalog/([a-z0-9-]+)/(\?.*)?$#Uusi', $_SERVER['REQUEST_URI']) && $_SERVER['SCRIPT_NAME']=='/bitrix/urlrewrite.php') {
	define('DETAIL_PRODUCT_PAGE', true);
}

# добавляем document root и путь до шаблона в include_path
$includePath = array(
	$_SERVER['DOCUMENT_ROOT'],
	realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..')
);
set_include_path(get_include_path() . PATH_SEPARATOR . implode(PATH_SEPARATOR, $includePath));