<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<section class="ourBrands blockWrapper clearfix">
	<h1>Brulex</h1>
	<h2>Наши бренды</h2>
	

	<div class="brulexBrand">
		<a class="brandLink" href="/catalog/?setFilter=Y&brand=181">&nbsp;</a>
		<?$APPLICATION->IncludeFile(
			SITE_TEMPLATE_PATH.'/inc_areas/brulex.php',
			array(),
			array('MODE'=>'HTML')
		)?>
	</div>
	<div class="normexBrand">
		<a class="brandLink" href="/catalog/?setFilter=Y&brand=182">&nbsp;</a>
		<?$APPLICATION->IncludeFile(
			SITE_TEMPLATE_PATH.'/inc_areas/normex.php',
			array(),
			array('MODE'=>'HTML')
		)?>
	</div>
</section>