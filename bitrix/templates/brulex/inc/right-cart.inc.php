<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?if($USER->IsAuthorized()):?>

<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "right", array(
	"PATH_TO_BASKET" => "/order/",
	"PATH_TO_PERSONAL" => "/personal/",
	"SHOW_PERSONAL_LINK" => "Y"
	),
	false,
	array(
		'HIDE_ICONS' => 'Y'
	)
);?>

<?endif;?>