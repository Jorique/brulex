<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

# вычисляем группу пользователя
$arUser = CUser::GetByID($USER->GetID())->Fetch();
$arGroups = CUser::GetUserGroup($USER->GetID());
$userType = in_array(8, $arGroups) ? 'dealer' : 'registered';

# формируем имя для заголовка
$arName = array();
if($arUser['NAME']) $arName[] = $arUser['NAME'];
if($arUser['NAME'] && $arUser['SECOND_NAME']) $arName[] = $arUser['SECOND_NAME'];
if(empty($arName) && $arUser['LAST_NAME']) $arName[] = $arUser['LAST_NAME'];
if(empty($arName)) $arName[] = $arUser['LOGIN'];
$name = implode(' ', $arName);

# фото
$photo = $arUser['PERSONAL_PHOTO'] ? CFile::GetPath($arUser['PERSONAL_PHOTO']) : SITE_TEMPLATE_PATH.'/images/no_photo.jpg';

/*echo '<pre>';
print_r($arResult);
print_r($_POST);
echo '</pre>';*/
?>

<div class="bx-auth-profile">

<?ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>

<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
	<?=$arResult["BX_SESSION_CHECK"]?>
	<input type="hidden" name="lang" value="<?=LANG?>" />
	<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />


	<div class="profileWrapper clearfix">
		<div class="profileLeft">
			<img src="<?=$photo?>" alt=""/>
		</div>
		<div class="profileRight">
			<h2 class="profileHeader">Здравствуйте, <?=$name?>!</h2>

			<?if($userType=='dealer'):?>
				<p><strong>Принадлежность к дилеру:</strong> <?=$arUser['UF_DEALER_NAME']?></p>
			<?endif;?>

			<p><strong>Группа:</strong> <?=$userType=='dealer' ? 'Дилер' : 'Зарегистрированный пользователь'?></p>

			<?if(trim($arUser['UF_AXAPTA_ID'])):?>
				<p><strong>Уникальный идентификатор дилера из Axapta:</strong> <?=$arUser['UF_AXAPTA_ID']?></p>
			<?endif;?>

			<div class="greyBlock">
				<div class="fieldBlock">
					<label for="profileLogin">Логин</label>
					<input type="text" name="LOGIN" id="profileLogin" value="<?=$arResult["arUser"]["LOGIN"]?>"/>
				</div>
				<div class="fieldBlock">
					<label for="profilePhone">Контактный телефон</label>
					<input type="text" name="PERSONAL_PHONE" id="profilePhone" value="<?=$arUser['PERSONAL_PHONE']?>"/>
				</div>
				<div class="fieldBlock">
					<label for="profileEmail">E-mail</label>
					<input type="text" name="EMAIL" id="profileEmail" value="<?=$arResult["arUser"]["EMAIL"]?>"/>
				</div>
				<div class="fieldBlock">
					<label for="profilePassword">Пароль</label>
					<input type="password" name="NEW_PASSWORD" id="profilePassword"/>
				</div>
				<div class="fieldBlock">
					<label for="profilePasswordConfirm">Подтверждение пароля</label>
					<input type="password" name="NEW_PASSWORD_CONFIRM" id="profilePasswordConfirm"/>
				</div>
				<br>
				<?php
				$addEmails = '';
				if(sizeof($arResult['arUser']['UF_EMAILS'])) {
					$addEmails = implode(', ', $arResult['arUser']['UF_EMAILS']);
				}
				?>
				<div class="fieldBlock addEmailsBlock">
					<label for="profileAddEmails">Дополнительные e-mail'ы (через запятую)</label>
					<textarea name="UF_EMAILS[]" id="profileAddEmails"><?=$addEmails?></textarea>
				</div>
			</div>

			<input class="orangeButton" type="submit" name="save" value="Сохранить изменения">
		</div>
	</div>
</form>
</div>