<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
#echo '<pre>'.print_r($arParams,1).'</pre>';

CModule::IncludeModule('sale');
CModule::IncludeModule('catalog');
$list = CSaleBasket::GetList(
	array(),
	array(
		'FUSER_ID' => CSaleBasket::GetBasketUserID(),
		'LID' => SITE_ID,
		'ORDER_ID' => 'NULL',
		'CAN_BUY' => 'Y',
		'DELAY' => 'N'
	)
);

if(!function_exists('pluralForm')) {
	function pluralForm($n, $form1, $form2, $form5) {
		$n = abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20) return $form5;
		if ($n1 > 1 && $n1 < 5) return $form2;
		if ($n1 == 1) return $form1;
		return $form5;
	}
}

$priceCount = 0;
while($item = $list->Fetch()) $priceCount += $item['PRICE']*$item['QUANTITY'];

$output = array(
	'count' => $arResult['NUM_PRODUCTS'],
	'price' => $priceCount,
	'pricePrint' => FormatCurrency($priceCount, 'RUB'),
	'plural' => pluralForm($arResult['NUM_PRODUCTS'], 'Товар', 'Товара', 'Товаров')
);
echo json_encode($output);
?>