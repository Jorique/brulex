<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(!function_exists('pluralForm')) {
	function pluralForm($n, $form1, $form2, $form5) {
		$n = abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20) return $form5;
		if ($n1 > 1 && $n1 < 5) return $form2;
		if ($n1 == 1) return $form1;
		return $form5;
	}
}
?>

<div class="rightCart">
	<div class="orangeCartHeader"><span></span>В корзине</div>
	<div class="rightCartInner">
		<div class="rightCartCount cartPhCount"><?=$arResult['NUM_PRODUCTS']?></div>
		<a href="<?=$arParams['PATH_TO_BASKET']?>" class="cartPhPlural"><?=pluralForm($arResult['NUM_PRODUCTS'], 'Товар', 'Товара', 'Товаров')?></a>
	</div>
</div>