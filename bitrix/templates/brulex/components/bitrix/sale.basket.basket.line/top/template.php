<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
#echo '<pre>'.print_r($arParams,1).'</pre>';

CModule::IncludeModule('sale');
CModule::IncludeModule('catalog');
$list = CSaleBasket::GetList(
	array(),
	array(
		'FUSER_ID' => CSaleBasket::GetBasketUserID(),
		'LID' => SITE_ID,
		'ORDER_ID' => 'NULL',
		'CAN_BUY' => 'Y',
		'DELAY' => 'N'
	)
);

$priceCount = 0;
while($item = $list->Fetch()) $priceCount += $item['PRICE']*$item['QUANTITY'];
?>

<div class="headerCartInner">
	<div class="headerCartContent">
		Товаров в корзине: <span class="orangeBold"><span class="cartPhCount"><?=$arResult['NUM_PRODUCTS']?></span> шт.</span><br>
		На сумму: <span class="orangeBold cartPhPricePrint"><?=FormatCurrency($priceCount, 'RUB')?></span><br>
		<a href="<?=$arParams['PATH_TO_BASKET']?>">Оформить заказ</a>
	</div>
</div>