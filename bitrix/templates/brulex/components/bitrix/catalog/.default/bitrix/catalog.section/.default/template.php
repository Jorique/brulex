<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
#echo '<pre>'.print_r($arResult,1).'</pre>';
if(!function_exists('getBrandLogo')) {
	function getBrandLogo($brandXmlId) {
		$basePath = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR;
		$baseUrl = '/images/';
		$extensions = array('png', 'jpg', 'jpeg', 'gif');
		foreach($extensions as $ext) {
			$filePathLC = $basePath.$brandXmlId.'.'.$ext;
			$filePathUC = $basePath.$brandXmlId.'.'.strtoupper($ext);
			if(file_exists($filePathLC) && is_readable($filePathLC)) {
				return $baseUrl.$brandXmlId.'.'.$ext;
			}
			elseif(file_exists($filePathUC) && is_readable($filePathUC)) {
				return $baseUrl.$brandXmlId.'.'.strtoupper($ext);
			}
		}
		return false;
	}
}

# вычисляем, является ли товар акцией для текущего покупателя
if(!function_exists('isShares')) {
	function isShares($article, $arUserGroups) {
		$rsPossible = CIBlockElement::GetList(
			array(),
			array(
				'IBLOCK_ID' => 4,
				'NAME' => $article,
				'ACTIVE' => 'Y'
			),
			false,
			false,
			array(
				'ID',
				'NAME',
				'CODE'
			)
		);
		if($rsPossible->SelectedRowsCount()) {
			while($arPossible = $rsPossible->Fetch()) {
				# вычисляем id группы
				$rsGroup = CGroup::GetList($by='id', $order='asc', array('NAME'=>'Internal-'.$arPossible['CODE']));
				if(!$rsGroup->SelectedRowsCount()) {
					return false;
				}
				$arGroup = $rsGroup->Fetch();

				if(in_array($arGroup['ID'], $arUserGroups)) {
					return true;
				}
			}
		}
		return false;
	}
}
$arUserGroups = CUser::GetUserGroup($USER->GetID());
?>

<?if($arResult['ITEMS']):?>
	<table class="catalogItems">
		<tr>
			<th>Новинка/Акция</th>
			<th>Название</th>
			<th>Цена</th>
			<th>Упаковка</th>
			<th>Кол-во упаковок</th>
			<th>Бренд</th>
			<th>Артикул</th>
			<th>Действие</th>
		</tr>
		<?foreach($arResult['ITEMS'] as $arElement):?>
			<?php
			$priceVal = 0;
			$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
			?>
			<tr id="<?=$this->GetEditAreaId($arElement['ID']);?>">
				<td class="tac">
					<nobr>
						<?if($arElement['PROPERTIES']['NEW']['VALUE']=='Y'):?>
							<img src="<?=SITE_TEMPLATE_PATH?>/images/new_icon.jpg" alt="Новинка" title="Новинка"/>
						<?endif;?>
						<?if($arElement['PROPERTIES']['SHARES']['VALUE']=='Y' || isShares($arElement['PROPERTIES']['ARTICLE']['VALUE'], $arUserGroups)):?>
							<img src="<?=SITE_TEMPLATE_PATH?>/images/sale_icon.jpg" alt="Акция" title="Акция"/>
						<?endif;?>
					</nobr>
				</td>
				<td><a class="detailLink" data-id="<?=$arElement['ID']?>" href="<?=$arElement['DETAIL_PAGE_URL']?>"><?=$arElement['NAME']?></a></td>
				<td class="tac">
					<?if($USER->IsAuthorized()):?>
						<?foreach($arElement["PRICES"] as $code=>$arPrice):?>
							<?if($arPrice["CAN_ACCESS"]):?>
								<?php
								# выводим цену за базовую единицу измерения
								$mpl = (float)$arElement['PROPERTIES']['MPL']['VALUE'];
								if($mpl > 0) {
									$arPrice['DISCOUNT_VALUE'] = round($arPrice['DISCOUNT_VALUE']/$mpl, 2);
									$arPrice['VALUE'] = round($arPrice['VALUE']/$mpl, 2);
								}
								$priceVal = $arPrice['VALUE'];
								?>

								<nobr>
								<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
									<s><?=FormatCurrency($arPrice['VALUE'], 'RUB')?></s> <span class="catalog-price"><?=FormatCurrency($arPrice['DISCOUNT_VALUE'], 'RUB')?></span><!--
								<?else:?>
									<span class="catalog-price"><?=FormatCurrency($arPrice['VALUE'], 'RUB')?></span><!--<?endif;?>
									-->/<?=$arElement['PROPERTIES']['UNIT']['VALUE']?>
								<?endif;?>
								</nobr>
						<?endforeach;?>
					<?else:?>
						Авторизуйтесь
					<?endif;?>
				</td>
				<td class="tac"><nobr><?=$arElement['PROPERTIES']['MPL']['VALUE']?> <?=$arElement['PROPERTIES']['UNIT']['VALUE']?></nobr></td>
				<td class="tac">
					<div class="quantityControls">
						<button class="quantityMinus"><span>&ndash;</span></button>
						<input class="quantityValue" type="text" value="1" data-validation="numeric" data-min="1" data-max="1000" />
						<button class="quantityPlus"><span>+</span></button>
					</div>
				</td>
				<td class="tac">
					<?php
					$img = getBrandLogo($arElement['PROPERTIES']['BRAND']['VALUE_XML_ID']);
					if($img) {
						echo '<img src="'.$img.'" alt="'.$arElement['PROPERTIES']['BRAND']['VALUE'].'">';
					}
					else {
						echo $arElement['PROPERTIES']['BRAND']['VALUE'];
					}
					?>
				</td>
				<td><?=$arElement['PROPERTIES']['ARTICLE']['VALUE']?></td>
				<td class="tar">
					<?if($priceVal):?>
						<a data-id="<?=$arElement['ID']?>" data-quantity="1" href="<?=$APPLICATION->GetCurDir()?>?<?=$arParams['ACTION_VARIABLE']?>=ADD2BASKET&id=<?=$arElement['ID']?>" class="orangeButton toCart"><span></span>В корзину</a>
					<?else:?>
						<a class="orangeButton toCart blocked" href="javascript:void(0);"><span></span>В корзину</a>
					<?endif;?>
				</td>
			</tr>
		<?endforeach;?>
	</table>
	<!--div class="actionDesc">
		<div class="actionCircleMin">
			<div class="newCircle"></div>
		</div>
		<span>Новинка</span>
		<div class="actionCircleMin">
			<div class="sharesCircle"></div>
		</div>
		<span>Акция</span>
	</div-->
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<div class="catalogNavi"><?=$arResult["NAV_STRING"]?></div>
	<?endif;?>
<?else:?>
	<p>Товаров не найдено.</p>
<?endif;?>