<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
if (!defined('NOT_SHOW_TITLE')) {
    define('NOT_SHOW_TITLE', true);
}
$ieObject = new CIBlockElement;
$priceVal = 0;

#echo '<pre>'.print_r($arResult,1).'</pre>';
?>

<div class="detailProduct clearfix">
    <div class="detailLeft">
        <?php
        $folderImage = getImageFromFolder($arResult['PROPERTIES']['ARTICLE']['VALUE']);
        if($arResult['DETAIL_PICTURE']) {
	        $pic = $arResult['DETAIL_PICTURE']['SRC'];
        }
        elseif($folderImage) {
	        $pic = $folderImage;
        }
        else {
	        $pic = SITE_TEMPLATE_PATH . '/images/no_photo.jpg';
        }
        //$pic = $arResult['DETAIL_PICTURE'] ? $arResult['DETAIL_PICTURE']['SRC'] : SITE_TEMPLATE_PATH . '/images/no_photo.jpg';
        ?>
        <img class="detailPhoto" src="<?= $pic ?>" alt="<?= $arResult['NAME'] ?>"/>
    </div>
    <div class="detailRight">
        <h1 class="detailHeader"><?= $arResult['NAME'] ?></h1>

        <p><strong>Цена:</strong>
            <? foreach ($arResult["PRICES"] as $code => $arPrice): ?>
            <? if ($arPrice["CAN_ACCESS"]): ?>
            <?php
            # выводим цену за базовую единицу измерения
            $mpl = (float)$arResult['PROPERTIES']['MPL']['VALUE'];
            if ($mpl > 0) {
                $arPrice['DISCOUNT_VALUE'] = round($arPrice['DISCOUNT_VALUE'] / $mpl, 2);
                $arPrice['VALUE'] = round($arPrice['VALUE'] / $mpl, 2);
            }
            ?>
        <nobr>
            <? if ($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]): ?>
            <s style="color: red;"><?= FormatCurrency($arPrice['VALUE'], 'RUB') ?></s> <span
            class="catalog-price"><?= FormatCurrency($arPrice['DISCOUNT_VALUE'], 'RUB') ?></span><!--
						<? else: ?>
							<span class="catalog-price"><?= FormatCurrency($arPrice['VALUE'], 'RUB') ?></span><!--
						<?
            endif; ?>
						-->/<?= $arResult['PROPERTIES']['UNIT']['VALUE'] ?>
        </nobr>
        <?php
        $priceVal = $arPrice['VALUE'];
        ?>
        <? endif; ?>
        <? endforeach; ?>
        </p>

        <p>
            <strong>Упаковка:</strong> <?= $arResult['PROPERTIES']['MPL']['VALUE'] ?> <?= $arResult['PROPERTIES']['UNIT']['VALUE'] ?>
        </p>

        <p><strong>Бренд:</strong> <?= $arResult['PROPERTIES']['BRAND']['VALUE'] ?></p>

        <? if ($priceVal): ?>
            <a data-id="<?= $arResult['ID'] ?>" data-quantity="1"
               href="<?= $APPLICATION->GetCurDir() ?>?<?= $arParams['ACTION_VARIABLE'] ?>=ADD2BASKET&id=<?= $arResult['ID'] ?>"
               class="orangeButton toCart toCartBig"><span></span>Добавить в корзину</a>
        <? else: ?>
            <a class="orangeButton toCart toCartBig blocked" href="javascript:void(0);"><span></span>Добавить в корзину</a>
        <?endif; ?>

        <? if ($arResult['DETAIL_TEXT']): ?>
            <p><strong>Описание:</strong> <?= $arResult['DETAIL_TEXT'] ?></p>
        <? endif; ?>

        <? if ($arResult['PROPERTIES']['ADD_GOODS']['VALUE']): ?>
            <div class="addGoodsBlock addProducts">
                <h2>С товаром также покупают</h2>

                <div class="addGoodsInner">
                    <div<?= sizeof($arResult['PROPERTIES']['ADD_GOODS']['VALUE']) > 4 ? ' class="carousel"' : '' ?>>
                        <ul>
                            <?php
                            foreach ($arResult['PROPERTIES']['ADD_GOODS']['VALUE'] as $addId) {
                                $el = $ieObject->GetList(
                                    array(),
                                    array(
                                        'IBLOCK_ID' => $arResult['IBLOCK_ID'],
                                        'ID'        => $addId
                                    ),
                                    false,
                                    false,
                                    array(
                                        'ID',
                                        'DETAIL_PAGE_URL',
                                        'NAME',
                                        'PREVIEW_PICTURE',
                                        'PROPERTY_MPL',
                                        'PROPERTY_UNIT',
	                                    'PROPERTY_ARTICLE'
                                    )
                                );
                                if ($el->SelectedRowsCount()) {
                                    $el = $el->GetNextElement();
                                    $el = $el->GetFields();
                                    $mpl = (float)$el['PROPERTY_MPL_VALUE'];
                                    $addPrice = CCatalogProduct::GetOptimalPrice($addId, 1, $USER->GetUserGroupArray());
                                    $addPrice = $addPrice['DISCOUNT_PRICE'];
                                    if ($mpl > 0) {
                                        $addPrice = round($addPrice / $mpl, 2);
                                    }

	                                $folderImage = getImageFromFolder($el['PROPERTY_ARTICLE_VALUE']);

	                                if($el['PREVIEW_PICTURE']) {
		                                $pic = CFile::GetPath($el['PREVIEW_PICTURE']);
	                                }
	                                elseif($folderImage) {
		                                $pic = $folderImage;
	                                }
	                                else {
		                                $pic = SITE_TEMPLATE_PATH . '/images/no_photo_small.jpg';
	                                }

	                                //$pic = $el['PREVIEW_PICTURE'] ? CFile::GetPath($el['PREVIEW_PICTURE']) : SITE_TEMPLATE_PATH . '/images/no_photo_small.jpg';
                                    ?>
                                    <li>
                                        <div class="agImage square"><a href="<?= $el['DETAIL_PAGE_URL'] ?>"><img
                                                    src="<?= $pic ?>" alt="<?= $el['NAME'] ?>"/></a></div>
                                        <div class="agName"><a
                                                href="<?= $el['DETAIL_PAGE_URL'] ?>"><?= $el['NAME'] ?></a></div>
                                        <?php
                                        $printPrice = FormatCurrency($addPrice, 'RUB') . '/' . $el['PROPERTY_UNIT_VALUE'];
                                        ?>
                                        <div class="agPrice"><strong>Цена:</strong> <?= $printPrice ?></div>
                                        <? if ($addPrice): ?>
                                            <a data-id="<?= $el['ID'] ?>" data-quantity="1"
                                               href="<?= $APPLICATION->GetCurDir() ?>?<?= $arParams['ACTION_VARIABLE'] ?>=ADD2BASKET&id=<?= $el['ID'] ?>"
                                               class="orangeButton toCart"><span></span>В корзину</a>
                                        <? else: ?>
                                            <a class="orangeButton toCart blocked"
                                               href="javascript:void(0);"><span></span>В корзину</a>
                                        <?endif; ?>
                                    </li>
                                <?
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <? if (sizeof($arResult['PROPERTIES']['ADD_GOODS']['VALUE']) > 4): ?>
                        <div class="carouselPrev"></div>
                        <div class="carouselNext"></div>
                    <? endif; ?>
                </div>
            </div>
        <? endif; ?>

        <? if ($arResult['PROPERTIES']['ALT_GOODS']['VALUE']): ?>
            <div class="addGoodsBlock altProducts">
                <h2>Альтернативные товары</h2>

                <div class="addGoodsInner">
                    <div<?= sizeof($arResult['PROPERTIES']['ALT_GOODS']['VALUE']) > 4 ? ' class="carousel"' : '' ?>>
                        <ul>
                            <?php
                            foreach ($arResult['PROPERTIES']['ALT_GOODS']['VALUE'] as $addId) {
                                $el = $ieObject->GetList(
                                    array(),
                                    array(
                                        'IBLOCK_ID' => $arResult['IBLOCK_ID'],
                                        'ID'        => $addId
                                    ),
                                    false,
                                    false,
                                    array(
                                        'ID',
                                        'DETAIL_PAGE_URL',
                                        'NAME',
                                        'PREVIEW_PICTURE',
                                        'PROPERTY_MPL',
                                        'PROPERTY_UNIT',
	                                    'PROPERTY_ARTICLE'
                                    )
                                );
                                if ($el->SelectedRowsCount()) {
                                    $el = $el->GetNextElement();
                                    $el = $el->GetFields();
                                    $mpl = (float)$el['PROPERTY_MPL_VALUE'];
                                    $addPrice = CCatalogProduct::GetOptimalPrice($addId, 1, $USER->GetUserGroupArray());
                                    $addPrice = $addPrice['DISCOUNT_PRICE'];
                                    if ($mpl > 0) {
                                        $addPrice = round($addPrice / $mpl, 2);
                                    }





	                                $folderImage = getImageFromFolder($el['PROPERTY_ARTICLE_VALUE']);
	                                if($el['PREVIEW_PICTURE']) {
		                                $pic = CFile::GetPath($el['PREVIEW_PICTURE']);
	                                }
	                                elseif($folderImage) {
		                                $pic = $folderImage;
	                                }
	                                else {
		                                $pic = SITE_TEMPLATE_PATH . '/images/no_photo_small.jpg';
	                                }
                                    //$pic = $el['PREVIEW_PICTURE'] ? CFile::GetPath($el['PREVIEW_PICTURE']) : SITE_TEMPLATE_PATH . '/images/no_photo_small.jpg';
                                    ?>



                                    <li>
                                        <div class="agImage square"><a href="<?= $el['DETAIL_PAGE_URL'] ?>"><img
                                                    src="<?= $pic ?>" alt="<?= $el['NAME'] ?>"/></a></div>
                                        <div class="agName"><a
                                                href="<?= $el['DETAIL_PAGE_URL'] ?>"><?= $el['NAME'] ?></a></div>
                                        <?php
                                        if ($USER->IsAuthorized()) {
                                            $printPrice = FormatCurrency($addPrice, 'RUB') . '/' . $el['PROPERTY_UNIT_VALUE'];
                                        } else {
                                            $printPrice = 'Авторизуйтесь';
                                            $addPrice = 0;
                                        }
                                        ?>
                                        <div class="agPrice"><strong>Цена:</strong> <?= $printPrice ?></div>
                                        <? if ($addPrice): ?>
                                            <a data-id="<?= $el['ID'] ?>" data-quantity="1"
                                               href="<?= $APPLICATION->GetCurDir() ?>?<?= $arParams['ACTION_VARIABLE'] ?>=ADD2BASKET&id=<?= $el['ID'] ?>"
                                               class="orangeButton toCart"><span></span>В корзину</a>
                                        <? else: ?>
                                            <a class="orangeButton toCart blocked"
                                               href="javascript:void(0);"><span></span>В корзину</a>
                                        <?endif; ?>
                                    </li>
                                <?
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <? if (sizeof($arResult['PROPERTIES']['ALT_GOODS']['VALUE']) > 4): ?>
                        <div class="carouselPrev"></div>
                        <div class="carouselNext"></div>
                    <? endif; ?>
                </div>
            </div>
        <? endif; ?>

    </div>
</div>


<? return; ?>
