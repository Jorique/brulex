<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arResult["isFormErrors"] == "Y") {
	$output = array(
		'success' => false,
		'errors' => $arResult["FORM_ERRORS_TEXT"]
	);
	echo json_encode($output);
	return;
}
if(trim($arResult["FORM_NOTE"])) {
	$message = 'Спасибо!<br> Ваша заявка на регистрацию принята.';
	$output = array(
		'success' => true,
		'message' => $message
	);
	echo json_encode($output);
	return;
}
$submitValue = htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);
?>

<?#='<pre>'.print_r($arResult,1).'</pre>'?>
<?#='<pre>'.print_r($_POST,1).'</pre>'?>

<?if($arResult["isFormNote"] != "Y"):?>
	<div class="registerRequest rqForm">
		<div class="registerRequestTriangle"></div>
		<div class="registerRequestInner">
			<?=$arResult["FORM_HEADER"]?>
			<input class="iconInput registerRequestFio" type="text" name="form_text_1" placeholder="Ф. И. О." />
			<input class="iconInput registerRequestPhone" type="text" name="form_text_2" placeholder="Телефон" />
			<input class="iconInput registerRequestMail" type="text" name="form_email_3" placeholder="E-mail" />
			<input type="hidden" name="web_form_submit" value="<?=$submitValue?>" />
			<input class="orangeButton" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=$submitValue?>" />
			<?=$arResult["FORM_FOOTER"]?>
		</div>
	</div>
<?endif;?>