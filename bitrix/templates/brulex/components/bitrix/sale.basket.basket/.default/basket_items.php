<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
#echo '<pre>'.print_r($arResult, 1).'</pre>';
echo ShowError($arResult["ERROR_MESSAGE"]);

CModule::IncludeModule('iblock');
$ieObject = new CIBlockElement;
?>

<table class="cartItems">
	<tr>
		<th>Название</th>
		<th>Цена</th>
		<th>Упаковка</th>
		<th>Кол-во упаковок</th>
		<th>Стоимость</th>
		<th>Действие</th>
	</tr>
	<?foreach($arResult["ITEMS"]["AnDelCanBuy"] as $arBasketItems):?>
		<?php
		# забираем единицы измерения
		$rsProps = $ieObject->GetList(
			array(),
			array(
				'IBLOCK_ID' => 3,
				'ID' => $arBasketItems['PRODUCT_ID']
			),
			false,
			false,
			array(
				'ID',
				'PROPERTY_MPL',
				'PROPERTY_UNIT'
			)
		);
		$arProps = $rsProps->Fetch();
		$mpl = (float)$arProps['PROPERTY_MPL_VALUE'];
		?>
		<tr>
			<td>
				<?if (strlen($arBasketItems["DETAIL_PAGE_URL"])>0):?><a class="detailLink" href="<?=$arBasketItems["DETAIL_PAGE_URL"] ?>"><?endif;?>
				<?=$arBasketItems["NAME"]?>
				<?if (strlen($arBasketItems["DETAIL_PAGE_URL"])>0):?></a><?endif;?>
			</td>
			<td class="tac">
				<?php
				$price = (float)$arBasketItems['PRICE'];
				$oldPrice = (float)$arBasketItems['DISCOUNT_PRICE'] ? round($arBasketItems['PRICE']+$arBasketItems['DISCOUNT_PRICE'], 2) : $price;
				if($mpl>0) {
					$price = round($price/$mpl, 2);
					$oldPrice = round($oldPrice/$mpl, 2);
				}
				if($oldPrice>$price) {
					echo '<s style="color: red;">'.FormatCurrency($oldPrice, 'RUB').'</s> ';
				}
				echo FormatCurrency($price, 'RUB').'/'.$arProps['PROPERTY_UNIT_VALUE'];
				?>
			</td>
			<td class="tac"><?=$arProps['PROPERTY_MPL_VALUE']?> <?=$arProps['PROPERTY_UNIT_VALUE']?></td>
			<td class="tac">
				<div class="quantityControls">
					<button class="quantityMinus"><span>&ndash;</span></button>
					<input data-id="<?=$arBasketItems['ID']?>" data-price="<?=$arBasketItems['PRICE']?>" class="quantityValue" type="text" value="<?=$arBasketItems["QUANTITY"]?>" data-validation="numeric" data-min="1" data-max="1000" name="QUANTITY_<?=$arBasketItems["ID"] ?>" />
					<button class="quantityPlus"><span>+</span></button>
				</div>
			</td>
			<td class="tac"><span class="itemSum"><?=FormatCurrency((float)$arBasketItems['PRICE']*$arBasketItems["QUANTITY"], 'RUB')?></span></td>
			<td class="tac"><a data-id="<?=$arBasketItems['ID']?>" class="orangeButton removeFromCart" href="javascript:void(0);">Убрать</a></td>
		</tr>
	<?endforeach;?>
</table>
<div class="cartSummary">Итого: <span class="cartPhPricePrint"><?=$arResult["allSum_FORMATED"]?></span></div>