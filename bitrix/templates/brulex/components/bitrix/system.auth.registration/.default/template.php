<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die;?>

<?ShowMessage($arParams["~AUTH_RESULT"]);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:main.register",
	".default",
	Array(
		"SHOW_FIELDS" => array(0=>"PERSONAL_PHONE",),
		"REQUIRED_FIELDS" => array(0=>"PERSONAL_PHONE",),
		"AUTH" => "N",
		"USE_BACKURL" => "Y",
		"SUCCESS_PAGE" => "",
		"SET_TITLE" => "N",
		"USER_PROPERTY" => array(0=>"UF_TYPE",1=>"UF_EMAILS"),
		"USER_PROPERTY_NAME" => ""
	)
);?>

<p><a href="<?=$arResult["AUTH_AUTH_URL"]?>" rel="nofollow"><b><?=GetMessage("AUTH_AUTH")?></b></a></p>