<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arResult["isFormErrors"] == "Y") {
	$output = array(
		'success' => false,
		'errors' => $arResult["FORM_ERRORS_TEXT"]
	);
	echo json_encode($output);
	return;
}
if(trim($arResult["FORM_NOTE"])) {
	$message = 'Спасибо!<br> Ваш заказ принят.';
	$output = array(
		'success' => true,
		'message' => $message
	);
	echo json_encode($output);
	return;
}
$submitValue = htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);
?>

<?#='<pre>'.print_r($arResult,1).'</pre>'?>
<?#='<pre>'.print_r($_POST,1).'</pre>'?>

<?if($arResult["isFormNote"] != "Y"):?>
	<section class="registerRequest callRequest">
		<div class="registerRequestTriangle"></div>
		<div class="registerRequestInner">
			<?=$arResult["FORM_HEADER"]?>
			<input class="iconInput registerRequestFio" type="text" name="form_text_18" placeholder="Ф. И. О." />
			<input class="iconInput registerRequestPhone" type="text" name="form_text_19" placeholder="Телефон" />

			<select class="inputselect" name="form_dropdown_SIMPLE_QUESTION_461" id="form_dropdown_SIMPLE_QUESTION_461">
				<option value="">Тема звонка</option>
				<option value="20">Продажи / отгрузка / доставка</option>
				<option value="21">Техническая поддержка</option>
				<option value="22">Колористика</option>
				<option value="23">Прочее</option>
			</select>

			<input type="hidden" name="web_form_submit" value="<?=$submitValue?>" />
			<input class="orangeButton" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=$submitValue?>" />
			<?=$arResult["FORM_FOOTER"]?>
		</div>
	</section>
<?endif;?>