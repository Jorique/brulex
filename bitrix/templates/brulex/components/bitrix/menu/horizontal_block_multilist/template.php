<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die;

if(!empty($arResult)) {
	$prevLevel = 0;
	echo '<nav class="topMenu"><ul>';
	foreach($arResult as $arItem) {
		$prevLevel && $arItem['DEPTH_LEVEL']<$prevLevel && print(str_repeat('</ul></div></li>', $prevLevel-$arItem['DEPTH_LEVEL']));
		echo '<li'.($arItem['SELECTED'] ? ' class="selected"' : '').'><div><a href="'.$arItem['LINK'].'">'.$arItem['TEXT'].'</a>';
		echo $arItem['IS_PARENT'] ? '<ul>' : '</div></li>';
		$prevLevel = $arItem['DEPTH_LEVEL'];
	}
	$prevLevel>1 && print(str_repeat('</ul></div></li>', $prevLevel-1));
	echo '</ul></nav>';
}
?>