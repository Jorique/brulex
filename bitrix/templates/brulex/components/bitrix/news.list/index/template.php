<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<section class="indexNews blockWrapper clearfix">
	<h2>Новости компании</h2>
	<div class="content">
	<?foreach($arResult["ITEMS"] as $arItem):?>

		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>

		<article class="indexNewsItem" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<time><?=$arItem['DISPLAY_ACTIVE_FROM']?></time>
			<h3><?=$arItem['NAME']?></h3>

			<?=$arItem['PREVIEW_TEXT']?>

			<p><a href="<?=$arItem['DETAIL_PAGE_URL']?>">Читать полностью</a></p>
		</article>

	<?endforeach;?>
	</div>
</section>
