$(function() {
	// аналог php number_format
	var numberFormat = function(number, decimals, dec_point, thousands_sep) {
		number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
		var n = !isFinite(+number) ? 0 : +number,
			prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
			sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
			dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
			s = '',
			toFixedFix = function (n, prec) {
				var k = Math.pow(10, prec);
				return '' + Math.round(n * k) / k;
			};
		// Fix for IE parseFloat(0.55).toFixed(0) = 0;
		s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
		if (s[0].length > 3) {
			s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		}
		if ((s[1] || '').length < prec) {
			s[1] = s[1] || '';
			s[1] += new Array(prec - s[1].length + 1).join('0');
		}
		return s.join(dec);
	}

	// слайдер на главной
	$('.indexSlider').scrollable({
		items: '.scrollableItems',
		circular: true,
		next: '.indexSliderRight',
		prev: '.indexSliderLeft'
	}).autoscroll(5000).navigator();

	// кастомизированные чекбоксы
	$('.ie8 .checkbox').on('change', function() {
		if( $(this).is(':checked') ) {
			$(this).next('label').addClass('checked');
		}
		else {
			$(this).next('label').removeClass('checked');
		}
	}).trigger('change');

	// карусели на детальной товара
	$('.addProducts .carousel').jCarouselLite({
		btnNext: '.addProducts .carouselNext',
		btnPrev: '.addProducts .carouselPrev',
		auto: 7000,
		circular: true,
		visible: 4,
		scroll: 1
	});
	$('.altProducts .carousel').jCarouselLite({
		btnNext: '.altProducts .carouselNext',
		btnPrev: '.altProducts .carouselPrev',
		auto: 7000,
		circular: true,
		visible: 4,
		scroll: 1
	});

	// +/- количества товара
	$('[data-validation="numeric"]').numeric({
		negative: false,
		decimal: false
	});

	$('.quantityMinus').on('click', function() {
		if( $(this).is('.blocked') ) return false;
		var $field = $(this).parent('.quantityControls').find('.quantityValue'),
			min = $field.data('min'),
			val = parseInt($field.val());
		if(isNaN(val) || val==min) return false;
		$field.val(val>min ? val-1 : val).trigger('change');
		return false;
	});

	$('.quantityPlus').on('click', function() {
		if( $(this).is('.blocked') ) return false;
		var $field = $(this).parent('.quantityControls').find('.quantityValue'),
			max = $field.data('max'),
			val = parseInt($field.val());
		if(isNaN(val) || val==max) return false;
		$field.val(val<max ? val+1 : val).trigger('change');
		return false;
	});

	$('.catalogItems .quantityValue').on('change', function() {
		var val = parseInt($(this).val()),
			$button = $(this).parents('tr').eq(0).find('.toCart');
		if(isNaN(val)) return;
		$button.data('quantity', val);
	});

	// ajax-корзина
	var updateCartInfo = function(resp, button) {
		if(!resp.count || !resp.price || !resp.pricePrint || !resp.plural) return;
		$('.cartPhCount').html(resp.count);
		$('.cartPhPricePrint').html(resp.pricePrint);
		$('.cartPhPlural').html(resp.plural);
		button.removeClass('blocked');
	}

	$(document).on('click', '.toCart', function() {
		if( $(this).is('.blocked') ) return false;
		$(this).addClass('blocked');

		var button = $(this);
		$.post(
			'/bitrix/templates/brulex/ajax/connector.php',
			{
				act: 'cart',
				action: 'BUY',
				quantity: $(this).data('quantity'),
				id: $(this).data('id')
			},
			function(resp) {
				updateCartInfo(resp, button);
				$.jGrowl('Товар добавлен в корзину', {theme: 'successGrowl', life: 5000});
			},
			'json'
		);
		return false;
	});

	$('.cartItems .quantityValue').on('change', function() {
		var button = $(this).parents('tr').eq(0).find('.quantityPlus, .quantityMinus'),
			price = $(this).data('price'),
			quantity = $(this).val(),
			$sum = $(this).parents('tr').eq(0).find('.itemSum');
		button.addClass('blocked');
		$.post(
			'/bitrix/templates/brulex/ajax/connector.php',
			{
				act: 'quantity',
				id: $(this).data('id'),
				quantity: quantity

			},
			function(resp) {
				updateCartInfo(resp, button);
				//var newPrice = Math.round(price*quantity);
				var newPrice = (price*quantity).toFixed(2);
				newPrice = numberFormat(newPrice, 2, ',', ' ');
				$sum.html(newPrice+' руб.');
			},
			'json'
		);
	});

	$('.removeFromCart').on('click', function() {
		if( $(this).is('.blocked') ) return false;
		$(this).addClass('blocked');

		var button = $(this);
		$.post(
			'/bitrix/templates/brulex/ajax/connector.php',
			{
				act: 'quantity',
				id: $(this).data('id'),
				quantity: 0

			},
			function(resp) {
				if(!resp.count) {
					location.href = location.href;
				}
				updateCartInfo(resp, button);
				button.parents('tr').eq(0).fadeOut(300, function() {
					$(this).remove();
				});
			},
			'json'
		);
	});

	// fancybox
	$('.catalogItems .detailLink').on('click', function() {
		$.fancybox.open({
			type: 'ajax',
			href: '/bitrix/templates/brulex/ajax/connector.php?act=product&id='+$(this).data('id'),
			padding: 25
		});
		return false;
	});

	// заявка на регистрацию
	$('.registerLink').on('click', function(e) {
		var $regBlock = $('.rqForm');
		if( $regBlock.is(':visible') ) {
			$regBlock.hide();
			return false;
		}
		$regBlock.show();
		return false;
	});

	$(document).on('click', function() {
		$('.registerRequest').hide();
	});

	$('.registerRequest').on('click', function(e) {
		e.stopPropagation();
	});



	$('.registerRequest form').on('submit', function() {
		var that = this,
			$wrapper = $(this).parents('.registerRequest'),
			$orangeButton = $wrapper.find('.orangeButton');

		$orangeButton.attr('disabled', true);
		$.post(
			'/bitrix/templates/brulex/ajax/connector.php',
			//$(this).serialize(),
			//$.extend({act: 'register_request'}, $(this).serialize()),
			'act=register_request&'+$(this).serialize(),
			function(resp) {
				if(resp.success) {
					$.jGrowl(resp.message, {theme: 'successGrowl', life: 5000});
					$wrapper.hide(0, function() {
						$(this).remove();
					});
				}
				else if(resp.errors) {
					$.jGrowl(resp.errors, {theme: 'errorGrowl', life: 5000});
					$orangeButton.removeAttr('disabled');
				}
				else {
					$.jGrowl('Ошибка отправки формы', {theme: 'errorGrowl', life: 5000});
					$orangeButton.removeAttr('disabled');
				}
				//console.log(resp);
			},
			'json'
		);
		return false;
	});

	// заказ звонка
	$('.headerCall a').on('click', function(e) {
		var $callBlock = $('.callRequest');
		if( $callBlock.is(':visible') ) {
			$callBlock.hide();
			return false;
		}
		$callBlock.show();
		return false;
	});
});