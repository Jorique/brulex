<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
require_once 'inc/template_helper.inc.php';
?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="ru"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="ru"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="ru"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	
	<title><?$APPLICATION->ShowTitle('browser_title')?></title>

	<script src="<?=SITE_TEMPLATE_PATH?>/js/modernizr-2.0.6.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-1.10.1.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.tools.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jcarousellite_1.0.1.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.numeric.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.jgrowl.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.fancybox.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.placeholder-enhanced.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/js/custom.js?1"></script>

	<?$APPLICATION->ShowHead()?>
</head>
<body>
<?$APPLICATION->ShowPanel()?>
<?require 'inc/right-cart.inc.php'?>
<div class="wrapper">
	<div class="innerWrapper">

		<?require 'inc/header.inc.php'?>
		<?require 'inc/index-slider.inc.php'?>

		<?if(defined('DI_MAIN_PAGE')):?>
			<?require 'inc/our-brands.inc.php'?>
			<?require 'inc/index-news.inc.php'?>
		<?endif;?>

		<section class="contentWrapper blockWrapper">
			<?if(defined('DI_MAIN_PAGE')):?>
				<h2>О нас</h2>
			<?else:?>
				<?require 'inc/breadcrumbs.inc.php'?>
				<?if(!defined('DETAIL_PRODUCT_PAGE')):?>
					<h1><?$APPLICATION->ShowTitle()?></h1>
				<?endif;?>
			<?endif;?>
			<div class="content">