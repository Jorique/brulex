<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
			</div>
		</section>
	</div>
	<div class="brick"></div>
</div>
<footer class="footer">
	<div class="innerFooter">
		<div class="bottomPhones">
			<div class="bottomPhone">
				<span>
				<?$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH.'/inc_areas/code1.php',
					array(),
					array(
						'NAME' => 'код телефона',
						'MODE'=>'text'
					)
				)?>
				</span>
				<div>
				<?$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH.'/inc_areas/phone1.php',
					array(),
					array(
						'NAME' => 'телефон',
						'MODE'=>'text'
					)
				)?>
				</div>
			</div>
			<div class="bottomFax">
				<span>
				<?$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH.'/inc_areas/code2.php',
					array(),
					array(
						'NAME' => 'код телефона',
						'MODE'=>'text'
					)
				)?>
				</span>
				<div>
				<?$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH.'/inc_areas/phone2.php',
					array(),
					array(
						'NAME' => 'телефон',
						'MODE'=>'text'
					)
				)?>
				</div>
			</div>
			<div class="bottomMail">
				<div>
				<a href="mailto:<? include $_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/inc_areas/email.php' ?>">
					<?$APPLICATION->IncludeFile(
						SITE_TEMPLATE_PATH.'/inc_areas/email.php',
						array(),
						array(
							'NAME' => 'e-mail',
							'MODE'=>'text'
						)
					)?>
				</a>
				</div>
			</div>
		</div>
		<div class="footerRight">
			<? require 'inc/bottom-menu.inc.php' ?>
			<div class="copyright">
				<?$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH.'/inc_areas/copyright.php',
					array(),
					array(
						'NAME' => 'копирайт'
					)
				)?>
			</div>
		</div>
	</div>
</footer>
</body>
</html>