<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

# события на регистрацию пользователей - деактивация и распределение по группам
require 'init/register.inc.php';

# события на присвоение групп пользователям после присвоения axapta id
require 'init/groups.inc.php';

# забирает изображения из папки /images/
function getImageFromFolder($article) {
	# вырезаем из артикула невалидные символы
	$article = preg_replace('#[\/:\*\?"<>\|]#', '', $article);

	$basePath = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR;
	$baseUrl = '/images/';
	$extensions = array('png', 'jpg', 'jpeg', 'gif');

	foreach($extensions as $ext) {
		$filePathLC = $basePath.$article.'.'.$ext;
		$filePathUC = $basePath.$article.'.'.strtoupper($ext);
		if(file_exists($filePathLC) && is_readable($filePathLC)) {
			return $baseUrl.$article.'.'.$ext;
		}
		elseif(file_exists($filePathUC) && is_readable($filePathUC)) {
			return $baseUrl.$article.'.'.strtoupper($ext);
		}
	}
	return false;
}

// $entity_id - имя объекта (у нас "BLOG_RATING")
// $value_id - идентификатор элемента (вероятно, ID элемента, свойство которого мы сохраняем или получаем. в нашем случае, это ID комментария)
// $uf_id - имя пользовательского свойства (в нашем случае UF_RATING)
// $uf_value - значение, которое сохраняем

# устанавливает значение пользовательского поля
function setUserField($entity_id, $value_id, $uf_id, $uf_value) {
	return $GLOBALS["USER_FIELD_MANAGER"]->Update(
		$entity_id,
		$value_id,
		array(
			$uf_id => $uf_value
		)
	);
}

# возвращает значение пользовательского поля
function getUserField($entity_id, $value_id, $uf_id) {
	$arUF = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields($entity_id, $value_id);
	return $arUF[$uf_id]["VALUE"];
}


# выбор e-mail для отправки формы заказа звонка
AddEventHandler('main', 'OnBeforeEventSend', 'callRequestEmailChange');
function callRequestEmailChange(&$arFields, &$arTemplate) {
	if($arFields['RS_FORM_ID'] != 3) return;

	$arRelations = array(
		'[20]' => 'sales@brulex.ru', # Продажи / отгрузка / доставка
		'[21]' => 'rodionov@brulex.ru, pryazhentsev@brulex.ru', # Техническая поддержка
		'[22]' => 'kolos@brulex.ru, pryazhentsev@brulex.ru', # Колористика
		'[23]' => 'sales@brulex.ru' # Прочее
	);

	$emailTo = 'sales@brulex.ru';
	foreach($arRelations as $optionId => $relEmail) {
		if(strpos($arFields['SIMPLE_QUESTION_461'], $optionId) !== false) {
			$emailTo = $relEmail;
			break;
		}
	}
	$arFields['EMAIL_TO'] = $emailTo;
	//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/_a.txt', print_r($arFields, 1));
}