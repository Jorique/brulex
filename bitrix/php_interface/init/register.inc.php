<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

function onBeforeUserRegister(&$arFields) {
	# деактивируем пользователя
	$arFields['ACTIVE'] = 'N';

	# распределяем по группам
	if($arFields['UF_TYPE']==1) {
		# если выбрано дилеры - определяем в соотв. группу, иначе в зарег. пользователи
		$arFields['GROUP_ID'] = array(8);
	}

	//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/___1.txt', print_r($arFields,1));
}

AddEventHandler("main", "OnBeforeUserRegister", "onBeforeUserRegister");