<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

AddEventHandler('main', 'OnBeforeUserAdd', array('AxaptaGroups', 'changeGroups'));
AddEventHandler('main', 'OnBeforeUserUpdate', array('AxaptaGroups', 'changeGroups'));

/**
 * Устанавливает или снимает группу, соответствующую AxaptaId после
 * добавления или обновления пользователя
 * Class AxaptaGroups
 */
class AxaptaGroups {
	static $prefix = 'Internal-';
	static $registeredGroupId = 5;
	static $dealerGroupId = 8;
	static $adminGroupId = 1;
	static $managerGroupId = 1157;
	static $arGroupIds = array();
	static $arParams = array();

	/**
	 * Собирает ид текущих групп в массив self::$arGroupIds
	 */
	private function collectGroupIds() {
		if(sizeof(self::$arParams['GROUP_ID'])) {
			foreach(self::$arParams['GROUP_ID'] as $arGroup) {
				self::$arGroupIds[] = $arGroup['GROUP_ID'];
			}
		}
		elseif(isset(self::$arParams['ID'])) {
			self::$arGroupIds = CUser::GetUserGroup(self::$arParams['ID']);
		}
	}

	private static function isDealer() {
		return in_array(self::$dealerGroupId, self::$arGroupIds);
	}

	private static function isAdmin() {
		return in_array(self::$adminGroupId, self::$arGroupIds);
	}

	private static function isManager() {
		return in_array(self::$managerGroupId, self::$arGroupIds);
	}

	private static function setBaseGroup() {
		$groupId = self::isDealer() ? self::$dealerGroupId : self::$registeredGroupId;
		self::$arParams['GROUP_ID'] = array(array('GROUP_ID'=>$groupId));
	}

	public static function changeGroups(&$arParams) {
		self::$arParams = &$arParams;
		self::collectGroupIds();

		# если админ - выходим
		if(self::isAdmin() || self::isManager()) return;

		# сразу устанавливаем пользователю только базовую группу
		self::setBaseGroup();
		if($arParams['UF_AXAPTA_ID']) {
			# ищем такую группу и вычисляем её ид
			$by = 'id';
			$order = 'asc';
			$rsGroup = CGroup::GetList($by, $order, array('NAME'=>self::$prefix.self::$arParams['UF_AXAPTA_ID']));
			if($rsGroup->SelectedRowsCount()) {
				$arGroup = $rsGroup->Fetch();
				$arParams['GROUP_ID'][] = array('GROUP_ID'=>$arGroup['ID']);
			}
		}
	}
}