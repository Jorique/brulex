<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оформление заказа");
?>

<?$APPLICATION->IncludeComponent(
	"uniweb:order",
	"",
	Array(
	),
	false,
	array(
		'HIDE_ICONS' => 'Y'
	)
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket",
	".default",
	Array(
		"COLUMNS_LIST" => "",
		"PATH_TO_ORDER" => "/personal/order.php",
		"HIDE_COUPON" => "N",
		"QUANTITY_FLOAT" => "N",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"USE_PREPAYMENT" => "N",
		"SET_TITLE" => "N"
	)
);?>

<?$APPLICATION->ShowViewContent("order")?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>