<?
if(is_array($_POST['UF_EMAILS']) && $_POST['UF_EMAILS'][0]) {
	$_POST['UF_EMAILS'] = preg_split('#\s*,\s*#', $_POST['UF_EMAILS'][0]);
}

define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"])>0) 
	LocalRedirect($backurl);

$APPLICATION->SetTitle("Вход на сайт");
?>
<p class="notetext">Вы зарегистрированы и успешно авторизовались.</p>

<p><a href="/">Вернуться на главную страницу</a></p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>